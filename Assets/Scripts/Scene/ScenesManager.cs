﻿/* ### ScenesManager Update Log ###
 * 2018.05.09 v1 ScenesManager.cs 생성
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScenesManager : MonoBehaviour {
	private static ScenesManager Instance;
	private Transform buttons;
	private string last_Scene;
	private string current_Scene;
	private int buttons_ChildCount;
	private bool start_Game;
	// Use this for initialization
	void Start () {
		if(Instance != null){
			Destroy(this.gameObject);
		}
		else{
			DontDestroyOnLoad(this.gameObject);
			start_Game = false;
			Instance = this;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 자신의 정보를 주는 Getter
	public static ScenesManager GetInstance(){
		return Instance;
	}

	// 어플리케이션을 종료하기 위한 함수
	public void ExitGame(){
		Application.Quit();
	}

	private void OnEnable(){
		SceneManager.sceneLoaded += UpdateButtons;
        SceneManager.sceneLoaded += UpdateBGM;
	}

	public void SceneChange(string sceneName){
		last_Scene = SceneManager.GetActiveScene().name;
		current_Scene = sceneName;
		SceneManager.LoadScene(sceneName);
	}

    public void reload()
    {
        PopupManager.GetInstance().PopupListner(0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void backScene()
    {
        PopupManager.GetInstance().PopupListner(0);
        SceneManager.LoadScene(last_Scene);
    }

    void UpdateBGM(Scene scene, LoadSceneMode mode)
    {
        if (start_Game || SceneManager.GetActiveScene().buildIndex != 0)
        {
            if (SceneManager.GetActiveScene().buildIndex < 4)
            {
                SoundManager.GetInstance().PlaySound(Define.SOUND_MUSIC.MENU_MUSIC);
            }
            else
            {
                SoundManager.GetInstance().PlaySound(Define.SOUND_MUSIC.GAME_MUSIC);
            }
			start_Game = true;
        }
    }

	private void UpdateButtons(Scene scene, LoadSceneMode mode){
		buttons = GameObject.Find("Buttons").transform;
		buttons_ChildCount = buttons.childCount;
		
		foreach (Button temp in buttons.GetComponentsInChildren<Button>())
        {
            temp.onClick.AddListener(delegate { SoundManager.GetInstance().PlaySound(Define.SOUND_EFFECT.BUTTON_INPUT); });
        }
		
		if(Equals(scene, SceneManager.GetSceneByName("Start"))){
			buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("IDPWMain"); });
			buttons.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("Memorizer"); });
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
		else if(Equals(scene, SceneManager.GetSceneByName("IDPWMain"))){
			buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("UserMain"); });
			buttons.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("MakeID"); });
			buttons.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("PasswordMain"); });
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("Start");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
		else if(Equals(scene, SceneManager.GetSceneByName("MakeID"))){
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("IDPWMain");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });

        }
		else if(Equals(scene, SceneManager.GetSceneByName("MakePWBank")) ||
				Equals(scene, SceneManager.GetSceneByName("MakePWInternet"))){
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("PasswordMain");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
		else if(Equals(scene, SceneManager.GetSceneByName("PasswordMain"))){
			buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("MakePWBank");});
			buttons.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("MakePWInternet");});
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("IDPWMain");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
		else if(Equals(scene, SceneManager.GetSceneByName("UserMain"))){
			buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("UserIDInformation");});
			buttons.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { SceneChange("UserPWInformation");});
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("IDPWMain");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
		else if(Equals(scene, SceneManager.GetSceneByName("UserIDInformation"))
		|| Equals(scene, SceneManager.GetSceneByName("UserPWInformation"))){
			buttons.GetChild(buttons_ChildCount - 2).GetComponent<Button>().onClick.AddListener(delegate{SceneChange("UserMain");});
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(Popup.EXIT); });
        }
        else if (Equals(scene, SceneManager.GetSceneByName("Memorizer")))
        {
            buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { PopupManager.GetInstance().PopupListner(4, "문제를 새로 만드시겠습니까", "확인", reload); });
            buttons.GetChild(buttons_ChildCount - 1).GetComponent<Button>().onClick.AddListener(delegate { backScene(); });
        }
	}

	public string GetCurrentScene(){
		return this.current_Scene;
	}
}
