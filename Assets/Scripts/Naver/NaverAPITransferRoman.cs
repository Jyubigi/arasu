﻿using UnityEngine;
using System;
using System.Net;
using System.Text;
using System.IO;

[System.Serializable]
public class Data{
	public Result[] aResult;
}

[System.Serializable]
public class Result{
	public string aFirstName;
	public Item[] aItems;
}

[System.Serializable]
public class Item{
	public string name;
	public int score;
}

public class NaverAPITransferRoman
{
	private const string CLIENT_ID = "KJpnKf7GnjHkx9OC3XyO";
	private const string CLIENT_SECRET = "Kce_MuCpmY";

	public string RequestRoman (string query)
	{
		if(!query.Equals("")){
			ServicePointManager.ServerCertificateValidationCallback = (p1, p2, p3, p4) => true;
			string url = "https://openapi.naver.com/v1/krdict/romanization?query=" + query;
			Debug.Log(query);
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Add("X-Naver-Client-Id", CLIENT_ID);
			request.Headers.Add("X-Naver-Client-Secret", CLIENT_SECRET);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Stream stream = response.GetResponseStream();
			StreamReader reader = new StreamReader(stream, Encoding.UTF8);
			string text = reader.ReadToEnd();
			Data d = JsonUtility.FromJson<Data>(text);
			Debug.Log(text);

			// 이상한 이름을 입력할 시 오류 체크
			if(d.aResult.Length == 0){
				return "-";
			}
			Debug.Log(d.aResult[0].aItems[0].name);
			
			return d.aResult[0].aItems[0].name;
		}
		// 보낼 값이 비어있으면, 빈값을 반환시킴.
		return "";
	}
}
