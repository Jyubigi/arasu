﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationManager : MonoBehaviour {
	private Transform rows, empty;
	private PlayerManager playerManger;

	// Use this for initialization
	void Start () {
		playerManger = PlayerManager.GetInstance();
		rows = this.transform.GetChild(2).GetChild(1);
		empty = this.transform.GetChild(3);

		this.Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		for(int i = 0; i < rows.childCount; i ++){
			rows.GetChild(i).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
			rows.GetChild(i).GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "";
			rows.GetChild(i).GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text = "";
			rows.GetChild(i).GetChild(2).GetChild(0).gameObject.GetComponent<Text>().text = "";
		}

		if(ScenesManager.GetInstance().GetCurrentScene().Equals("UserIDInformation")){
			if(playerManger.IsEmptyID()){
				empty.gameObject.SetActive(true);
			}
			else {
				this.MakeIDTable();
			}
		}
		else if(ScenesManager.GetInstance().GetCurrentScene().Equals("UserPWInformation")){
			if(playerManger.ISEmptyPW()){
				empty.gameObject.SetActive(true);
			}
			else {
				this.MakePWTable();
			}
		}
	}
	private void MakeIDTable(){
		for(int i = 0; i < playerManger.ID_Count; i ++){
			rows.GetChild(i).GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.ID_Type[i];
			rows.GetChild(i).GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.ID_Name[i];
			rows.GetChild(i).GetChild(2).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.ID[i];
			string idStr = playerManger.ID[i];
			int index = i;
			rows.GetChild(i).gameObject.GetComponent<Button>().onClick.AddListener(delegate{ PopupManager.GetInstance().PopupListner(Popup.TWOBTN, idStr + " 를 지우시겠습니까?", "삭제", delegate{RemoveIDRow(index);});});
		}
	}
	
	private void MakePWTable(){
		for(int i = 0; i < playerManger.PW_Count; i ++){
			rows.GetChild(i).GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.PW_Type[i];
			rows.GetChild(i).GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.PW_Origin[i];
			rows.GetChild(i).GetChild(2).GetChild(0).gameObject.GetComponent<Text>().text = playerManger.PW[i];
			string pwStr = playerManger.PW[i];
			int index = i;
			rows.GetChild(i).gameObject.GetComponent<Button>().onClick.AddListener(delegate{ PopupManager.GetInstance().PopupListner(Popup.TWOBTN, pwStr + " 를 지우시겠습니까?", "삭제", delegate{RemovePWRow(index);});});
		}
	}
	
	private void RemoveIDRow(int index){
		playerManger.RemoveID(index);
		this.Init();
		PopupManager.GetInstance().PopupListner(Popup.CLOSE);
	}

	private void RemovePWRow(int index){
		playerManger.RemovePW(index);
		this.Init();
		PopupManager.GetInstance().PopupListner(Popup.CLOSE);
	}
}
