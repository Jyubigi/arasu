﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
	private static PlayerManager Instance;
	private const int MAXCOUNT = 5;
	private int id_Count;
	private int pw_Count;
	private string[] id;
	private string[] id_Name;
	private string[] id_Type;
	private string[] pw;
	private string[] pw_Origin;
	private string[] pw_Type;

	public int ID_Count{
		get { return id_Count; }
		set { this.id_Count = value; }
	}

	public int PW_Count{
		get { return pw_Count; }
		set { this.id_Count = value; }
	}

	public string[] ID{
		get { return id; }
	}

	public string[] ID_Name{
		get { return id_Name; }
	}

	public string[] ID_Type{
		get { return id_Type; }
	}

	public string[] PW{
		get { return pw; }
	}

	public string[] PW_Origin{
		get { return pw_Origin; }
	}

	public string[] PW_Type{
		get { return pw_Type; }
	}

	// Use this for initialization
	void Start () {
		if(Instance != null){
			Destroy(this.gameObject);
		}
		else{
			DontDestroyOnLoad(this.gameObject);
			Instance = this;
			this.Init();
		}
	}

	public static PlayerManager GetInstance(){
		return Instance;
	}

	private void Init(){
		id = new string[MAXCOUNT];
		id_Name = new string[MAXCOUNT];
		id_Type = new string[MAXCOUNT];
		pw = new string[MAXCOUNT];
		pw_Origin = new string[MAXCOUNT];
		pw_Type = new string[MAXCOUNT];

		id_Count = PlayerPrefs.GetInt("idCount", 0);
		pw_Count = PlayerPrefs.GetInt("pwCount", 0);

		for(int i = 0; i < id_Count; i ++){
			id[i] = PlayerPrefs.GetString("id" + i, "");
			id_Name[i] = PlayerPrefs.GetString("idName" + i, "");
			id_Type[i] = PlayerPrefs.GetString("idType" + i, "");
		}
		for(int i = 0; i < pw_Count; i ++){
			pw[i] = PlayerPrefs.GetString("pw" + i, "");
			pw_Origin[i] = PlayerPrefs.GetString("pwOrigin" + i, "");
			pw_Type[i] = PlayerPrefs.GetString("pwType" + i, "");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool IsFullID(){
		return (id_Count >= MAXCOUNT) ? true : false;
	}

	public bool IsEmptyID(){
		return (id_Count == 0) ? true : false;
	}

	public bool IsFullPW(){
		return (pw_Count >= MAXCOUNT) ? true : false;
	}

	public bool ISEmptyPW(){
		return (pw_Count == 0) ? true : false;
	}

	public void SetIDAll(string type, string name, string id){
		if(IsFullID()){
			PopupManager.GetInstance().PopupListner(Popup.ONEBTN, Popup.id_Full);
		}
		else{
			SetIDType(type);
			SetIDName(name);
			SetID(id);
			IncreaseIDCount();
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.id_Create);
		}
	}
	
	public void SetID(string value){
		PlayerPrefs.SetString("id" + id_Count, value);
		this.id[id_Count] = value;
	}

	public void SetIDName(string value){
		PlayerPrefs.SetString("idName" + id_Count, value);
		this.id_Name[id_Count] = value;
	}

	public void SetIDType(string value){
		PlayerPrefs.SetString("idType" + id_Count, value);
		this.id_Type[id_Count] = value;
	}

	public void SetPWAll(string type, string origin, string pw){
		Debug.Log(pw_Count);
		if(IsFullPW()){
			PopupManager.GetInstance().PopupListner(Popup.ONEBTN, Popup.pw_Full);
		}
		else {
			SetPWType(type);
			SetPWOrigin(origin);
			SetPW(pw);
			IncreasePWCount();
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.pw_Create);
		}
	}

	public void SetPW(string value){
		PlayerPrefs.SetString("pw" + pw_Count, value);
		this.pw[pw_Count] = value;
	}

	public void SetPWOrigin(string value){
		PlayerPrefs.SetString("pwOrigin" + pw_Count, value);
		this.pw_Origin[pw_Count] = value;
	}

	public void SetPWType(string value){
		PlayerPrefs.SetString("pwType" + pw_Count, value);
		this.pw_Type[pw_Count] = value;
	}

	public void IncreaseIDCount(){
		this.id_Count ++;
		PlayerPrefs.SetInt("idCount", this.id_Count);
	}

	public void IncreasePWCount(){
		this.pw_Count ++;
		PlayerPrefs.SetInt("pwCount", this.pw_Count);
	}

	public void RemoveID(int index){
		for(int i = index + 1; i < id_Count; i ++){
			PlayerPrefs.SetString("id" + (i - 1), id[i]);
			PlayerPrefs.SetString("idName" + (i - 1), id_Name[i]);
			PlayerPrefs.SetString("idType" + (i - 1), id_Type[i]);
		}
		this.id_Count --;
		PlayerPrefs.DeleteKey("id" + id_Count);
		PlayerPrefs.DeleteKey("idName" + id_Count);
		PlayerPrefs.DeleteKey("idType" + id_Count);
		PlayerPrefs.SetInt("idCount", this.id_Count);

		this.Init();
	}

	public void RemovePW(int index){
		for(int i = index + 1; i < pw_Count; i ++){
			PlayerPrefs.SetString("pw" + (i - 1), pw[i]);
			PlayerPrefs.SetString("pwOrigin" + (i - 1), pw_Origin[i]);
			PlayerPrefs.SetString("pwType" + (i - 1), pw_Type[i]);
		}
		this.pw_Count --;
		PlayerPrefs.DeleteKey("pw" + pw_Count);
		PlayerPrefs.DeleteKey("pwOrigin" + pw_Count);
		PlayerPrefs.DeleteKey("pwType" + pw_Count);
		PlayerPrefs.SetInt("pwCount", this.pw_Count);

		this.Init();
	}
}
