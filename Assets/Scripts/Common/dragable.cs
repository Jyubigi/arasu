﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class dragable : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    int self;
    public Sprite dragImage;
    bool selected;
    private void Awake()
    {
        if(dragImage == null)
        {
            dragImage = this.GetComponent<Image>().sprite;
        }
        selected = false;
    }
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }
    public void setDragImage(Sprite dragImage)
    {
        this.dragImage = dragImage;
    }

    //Temporary Integer type if you need change self's type to Object
    public int getSelf()
    {
        return self;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!selected)
        {
            GamesManager.getInstance().setDragable(this);
            setSelect(true);
        }
        else
        {
            GamesManager.getInstance().setDragable(null);
            setSelect(false);
        }
    }

    public void setSelect(bool isSelect)
    {
        if (isSelect)
        {
            GetComponent<Image>().color = new Color(0, 1, 0);
            selected = true;
        }
        else
        {
            GetComponent<Image>().color = new Color(1, 1, 1);
            selected = false;
        }
    }
}