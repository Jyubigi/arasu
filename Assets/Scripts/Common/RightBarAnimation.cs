﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightBarAnimation : MonoBehaviour {
	Transform right, button;
	GameObject describe, describePref;
	Sprite normal, confused, sad, smile, correct, incorrect, notEnough, zero, next;

	// Use this for initialization
	void Start () {
		right = this.transform;
		button = right.GetChild(right.childCount - 1);
		describePref = Resources.Load<GameObject>("Pref/Common/Describe");
		describe = Instantiate<GameObject>(describePref, button);
		describe.SetActive(false);

		// 버튼 표정 Sprite
		normal = Resources.Load<Sprite>("Image/Common/Result/Result1");
		confused = Resources.Load<Sprite>("Image/Common/RightBar/confused");
		sad = Resources.Load<Sprite>("Image/Common/RightBar/sad");
		smile = Resources.Load<Sprite>("Image/Common/RightBar/smile");

		// 말풍선 Sprite
		correct = Resources.Load<Sprite>("Image/Common/RightBar/correct");
		incorrect = Resources.Load<Sprite>("Image/Common/RightBar/incorrect");
		notEnough = Resources.Load<Sprite>("Image/Common/RightBar/notEnough");
		zero = Resources.Load<Sprite>("Image/Common/RightBar/zero");
		next = Resources.Load<Sprite>("Image/Common/RightBar/next");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Correct(){
		StartCoroutine(ChangeImage(smile, correct));
	}

	public void Incorrect(){
		StartCoroutine(ChangeImage(sad, incorrect));
	}

	public void Zero(){
		StartCoroutine(ChangeImage(confused, zero));
	}
	
	public void NotEnough(){
		StartCoroutine(ChangeImage(confused, notEnough));
	}

	public void Next(){
		StartCoroutine(ChangeImage(smile, next));
	}

	IEnumerator ChangeImage(Sprite buttonSprite, Sprite comment){
		button.gameObject.GetComponent<Button>().enabled = false;
		button.gameObject.GetComponent<Image>().sprite = buttonSprite;
		describe.SetActive(true);
		describe.GetComponent<Image>().sprite = comment;
		yield return new WaitForSeconds(2.4f);
		button.gameObject.GetComponent<Image>().sprite = normal;
		describe.SetActive(false);
		button.gameObject.GetComponent<Button>().enabled = true;
	}
}
