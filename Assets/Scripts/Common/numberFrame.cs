﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class numberFrame : MonoBehaviour
{
    public static GameObject[] number_frames;

    private void Awake()
    {
    }
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void updateImage(Transform target, int number, Sprite[] numbers)
    {
        if (number < 10)
        {
            if (transform.childCount != 1)
            {
                GameObject temp = Instantiate<GameObject>(number_frames[0], transform.parent);
                temp.transform.localPosition = Vector3.zero;
                temp.GetComponent<numberFrame>().updateImage(target, number, numbers);
                Destroy(gameObject);
            }
            else
            {
                target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number];
            }
        }
        else if (number < 100)
        {
            if (transform.childCount != 2)
            {
                GameObject temp = Instantiate<GameObject>(number_frames[1], transform.parent);
                temp.transform.localPosition = Vector3.zero;
                temp.GetComponent<numberFrame>().updateImage(target, number, numbers);
                Destroy(gameObject);
            }
            else
            {
                target.GetChild(1).gameObject.GetComponent<Image>().sprite = numbers[number / 10];
                target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number % 10];
            }
        }
        else
        {
            target.GetChild(2).gameObject.GetComponent<Image>().sprite = numbers[number / 100];
            target.GetChild(1).gameObject.GetComponent<Image>().sprite = numbers[(number / 10) % 10];
            target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number % 10];
        }
    }
    public void updateImage(Transform target, int number, Sprite[] numbers, Vector2 size)
    {
        if (number < 10)
        {
            if (transform.childCount != 1)
            {
                target.GetChild(1).gameObject.SetActive(false);
                target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number % 10];
                target.GetChild(0).gameObject.GetComponent<RectTransform>().sizeDelta = size;
            }
            else
            {
                target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number];
                target.GetChild(0).gameObject.GetComponent<RectTransform>().sizeDelta = size;
            }
        }
        else if (number < 100)
        {
            target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number % 10];
            target.GetChild(0).gameObject.GetComponent<RectTransform>().sizeDelta = size;
            target.GetChild(1).gameObject.GetComponent<Image>().sprite = numbers[number / 10];
            target.GetChild(1).gameObject.GetComponent<RectTransform>().sizeDelta = size;
        }
        else
        {
            target.GetChild(0).gameObject.GetComponent<Image>().sprite = numbers[number % 10];
            target.GetChild(0).gameObject.GetComponent<RectTransform>().sizeDelta = size;
            target.GetChild(1).gameObject.GetComponent<Image>().sprite = numbers[(number / 10) % 10];
            target.GetChild(1).gameObject.GetComponent<RectTransform>().sizeDelta = size;
            target.GetChild(2).gameObject.GetComponent<Image>().sprite = numbers[number / 100];
            target.GetChild(2).gameObject.GetComponent<RectTransform>().sizeDelta = size;
        }
    }
    public void highlightImages(int mode)
    {
        foreach (Transform temp in GetComponentsInChildren<Transform>())
        {
            if (temp.gameObject != this.gameObject)
            {
                temp.gameObject.AddComponent<highLighting>();
                temp.gameObject.GetComponent<highLighting>().setMode(mode);
            }
        }
    }

    public static GameObject addNumber(int number, Vector2 position, Transform parent, Sprite[] numbers)
    {
        GameObject temp;

        if (number >= 100)
        {
            temp = Instantiate<GameObject>(number_frames[2], parent);
            temp.transform.localPosition = position;
        }
        else if (number >= 10)
        {
            temp = Instantiate<GameObject>(number_frames[1], parent);
            temp.transform.localPosition = position;
        }
        else
        {
            temp = Instantiate<GameObject>(number_frames[0], parent);
            temp.transform.localPosition = position;
        }
        temp.GetComponent<numberFrame>().updateImage(temp.transform, number, numbers);
        return temp;
    }
}