﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blankFrame : MonoBehaviour {

    public static GameObject[] blank_frames;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setCorrect(Transform target, int number, dragable[] dragables)
    {
        if (number < 10)
        {
            target.GetChild(1).gameObject.GetComponent<blank>().setCorrect(dragables[number]);
        }
        else if(number < 100)
        {
            target.GetChild(2).gameObject.GetComponent<blank>().setCorrect(dragables[number / 10]);
            target.GetChild(1).gameObject.GetComponent<blank>().setCorrect(dragables[number % 10]);
        }
        else
        {
            target.GetChild(3).gameObject.GetComponent<blank>().setCorrect(dragables[number / 100]);
            target.GetChild(2).gameObject.GetComponent<blank>().setCorrect(dragables[(number / 10) % 10]);
            target.GetChild(1).gameObject.GetComponent<blank>().setCorrect(dragables[number % 10]);
        }
    }

    public bool full()
    {
        blank[] childBlank = GetComponentsInChildren<blank>();
        bool result = true;
        foreach(blank temp in childBlank)
        {
            result &= (temp.answer != null);
        }
        return result;
    }

    public int getSelf()
    {
        int sum = 0;
        blank[] temp = GetComponentsInChildren<blank>();
        foreach(blank tempBlank in temp)
        {
            if(tempBlank.inputed == -1)
            {
                transform.root.SendMessage("notYet", null, SendMessageOptions.DontRequireReceiver);
                return -1;
            }
        }
        if (temp.Length == 1)
        {
            sum += temp[0].inputed;
        }
        else
        {
            sum += temp[0].inputed;
            sum += temp[1].inputed * 10;
        }
        return sum;
    }

    public void checkSelf()
    {
        blank[] temp = GetComponentsInChildren<blank>();
        Debug.Log(temp.Length);
        if (temp.Length == 1)
        {
            if (temp[0].inputed == 0)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            if (temp[1].inputed == 0)
            {
                Destroy(temp[1].gameObject);
                //checkSelf();
            }
        }
    }


    public static GameObject addBlank(int number, Vector2 position, Transform parent, dragable[] numberAnswers)
    {
        GameObject temp;
        if (number >= 100)
        {
            temp = Instantiate<GameObject>(blank_frames[2], parent);
            temp.transform.localPosition = position;
            temp.transform.GetChild(3).GetComponent<blank>().setAnswer(numberAnswers[number / 100], false);
            temp.transform.GetChild(3).GetComponent<blank>().enabled = false;
            temp.transform.GetChild(2).GetComponent<blank>().setAnswer(numberAnswers[(number / 10) % 10], false);
            temp.transform.GetChild(2).GetComponent<blank>().enabled = false;
        }
        else if (number >= 10)
        {
            temp = Instantiate<GameObject>(blank_frames[1], parent);
            temp.transform.localPosition = position;
            temp.transform.GetChild(2).GetComponent<blank>().setAnswer(numberAnswers[number / 10], false);
            temp.transform.GetChild(2).GetComponent<blank>().enabled = false;
        }
        else
        {
            temp = Instantiate<GameObject>(blank_frames[0], parent);
            temp.transform.localPosition = position;
        }
        temp.GetComponent<blankFrame>().setCorrect(temp.transform, number, numberAnswers);

        return temp;
    }
}
