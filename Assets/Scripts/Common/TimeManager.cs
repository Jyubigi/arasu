﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {

	private static TimeManager Instance;
	public float deltaTime = 0.0f;

	float lastFrame = 0;
	float currentFrame = 0;
	float myDelta = 0;

	public static TimeManager GetInstance()
    {
        return Instance;
    }

	private void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            GameObject.DontDestroyOnLoad(gameObject);
            Instance = this;
        }
    }

	public void Reset(){
		lastFrame = currentFrame = Time.realtimeSinceStartup;
		myDelta = Time.smoothDeltaTime;
	}
	
	// Update is called once per frame
	public void Update () {
		currentFrame = Time.realtimeSinceStartup;
		myDelta = currentFrame - lastFrame;
		myDelta *= Time.timeScale;
		lastFrame = currentFrame;

		//Pause
		if(Input.GetKeyDown(KeyCode.P))
		{
			Debug.Break();
		}
	}

	public void LateUpdate(){
		deltaTime = (Time.deltaTime + Time.smoothDeltaTime + myDelta) * 0.33333f;
	}
}
