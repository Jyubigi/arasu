﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class jumpFrog : MonoBehaviour
{

    [SerializeField]
    List<Transform> locations;
    List<Vector2> positions;
    GameObject originParent;
    Vector2 originPostion;
    [SerializeField]
    int current;
    int done;


    private void Awake()
    {
        originParent = transform.parent.gameObject;
        originPostion = transform.localPosition;
        current = 0;
        locations = new List<Transform>();
        positions = new List<Vector2>();
    }
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setPostions(List<Vector2> newPosition)
    {
        this.positions = newPosition;
    }

    public void setLocations(List<Transform> newLocation)
    {
        this.locations = newLocation;
        for (int i = 0; i < locations.Capacity; i++)
        {
            positions.Add(Vector2.zero);
        }
    }

    public void setLocations(List<Transform> newLocation, List<Vector2> newPosition)
    {
        this.locations = newLocation;
        this.positions = newPosition;
    }

    public void nextLocation()
    {
        int temp = current;

        while (!checkValid(temp))
        {
            temp++;
            if (temp >= locations.Count)
            {
                temp = -1;
                break;
            }
        }
        if (temp >= 0)
        {
            current = temp;
            transform.SetParent(locations[current]);
            transform.localPosition = positions[current];
            checkNext();
        }
        else
        {
            resetTransform();
        }
    }

    public void addLocation(Transform location)
    {
        this.locations.Add(location);
        this.positions.Add(Vector2.zero);
    }

    public void addLocation(Transform location, Vector2 position)
    {
        this.locations.Add(location);
        this.positions.Add(position);
    }

    bool checkValid(int location)
    {
        if (locations[location].GetComponent<blank>() != null)
        {
            return !locations[location].GetComponent<blank>().hasAnswer();
        }
        else if (locations[location].GetComponent<blankFrame>() != null)
        {
            bool temp = false;
            foreach (blank tempBlank in locations[location].GetComponentsInChildren<blank>())
            {
                temp = temp || !tempBlank.hasAnswer();
            }
            return temp;
        }
        else if (current == location)
        {
            return false;
        }

        return true;
    }

    public void resetTransform()
    {
        Debug.Log(originParent.transform);
        transform.SetParent(originParent.transform);
        transform.localPosition = originPostion;
    }

    public void clear()
    {
        locations = new List<Transform>();
        positions = new List<Vector2>();
        transform.GetChild(0).gameObject.SetActive(false);
        current = 0;
    }

    public void checkNext()
    {
        if (transform.parent.GetComponent<Button>() != null)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            Invoke("nextDone", 2.0f);
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    public void nextDone()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void saveDone()
    {
        done = current;
    }

    public void returnToDone()
    {
        current = done;
    }
}
