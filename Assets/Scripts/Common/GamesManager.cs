﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamesManager : MonoBehaviour {
    public dragable currentDragable;
    public blank currentBlank;
    
    private static GamesManager Instance;
    private GamesManager()
    {

    }
    private void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            GameObject.DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        numberFrame.number_frames = Resources.LoadAll<GameObject>("Prefabs/NumberFrame");
        blankFrame.blank_frames = Resources.LoadAll<GameObject>("Prefabs/BlankFrame");
    }
    public void Start(){
	}

    public void LateUpdate()
    {   
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PopupManager.GetInstance().PopupListner(Popup.EXIT);
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
        {
            if (this.currentDragable != null && this.currentBlank == null)
            {
                //this.currentDragable = null;
            }
            else if (this.currentDragable != null && this.currentBlank != null)
            {
                currentBlank.setAnswer(currentDragable);
                //this.currentDragable = null;
                setBlank(null);
            }
        }
#elif UNITY_ANDROID
        else if (Input.touchCount >= 1)
        {
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                if (this.currentDragable != null && this.currentBlank == null)
                {
                }
                else if (this.currentDragable != null && this.currentBlank != null)
                {
                    currentBlank.setAnswer(currentDragable);
                    setBlank(null);
                }
            }
        }
#endif
    }

    public static GamesManager getInstance()
    {
        return Instance;
    }

    public void setDragable(dragable data)
    {
        if (currentDragable == null && data == null)
        {

        }
        else if(data == null)
        {
            currentDragable.setSelect(false);
            currentDragable = null;
        }
        else
        {
            if (currentDragable != null)
            {
                currentDragable.setSelect(false);
            }
            this.currentDragable = data;
            data.setSelect(true);
        }
    }

    public void setBlank(blank data)
    {
        if (currentBlank == null && data == null)
        {

        }
        else if (data == null)
        {
            currentBlank.setSelect(false);
            currentBlank = null;
        }
        else
        {
            if (currentBlank != null)
            {
                currentBlank.setSelect(false);
            }
            this.currentBlank = data;
            data.setSelect(true);
        }
    }
    public void setBlank(blank data, bool isByBlanks)
    {
        if (currentBlank == null && data == null)
        {

        }
        else if (data == null)
        {
            currentBlank.setSelect(false, isByBlanks);
            currentBlank = null;
        }
        else
        {
            if(currentBlank != null)
            {
                currentBlank.setSelect(false, isByBlanks);
            }
            this.currentBlank = data;
            data.setSelect(true, isByBlanks);
        }
    }

    public blank getBlank()
    {
        return currentBlank;
    }
}
