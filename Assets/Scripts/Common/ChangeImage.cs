﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ChangeImage {
	public static void ChangeToImage(GameObject gameObject, Sprite sprite){
		gameObject.GetComponent<Image>().sprite = sprite;
	}
}
