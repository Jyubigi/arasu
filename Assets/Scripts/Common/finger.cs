﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class finger : MonoBehaviour {

    [SerializeField]
    Vector2[] positions;
    [SerializeField]
    float speed;
    [SerializeField]
    int currentPosition;
    [SerializeField]
    float[] times;
    [SerializeField]
    float time;
    int cycle, cycleTime;

    TimeManager timeManager;

    private void Awake()
    {
        timeManager = TimeManager.GetInstance();
        currentPosition = 0;
        speed = 300.0f;
        time = 0;
        cycle = 1;
        cycleTime = 0;
    }
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (cycle <= cycleTime)
        {
            Destroy(gameObject);
        }
        if (time > times[currentPosition])
        {
            if (currentPosition < positions.Length - 1)
            {
                transform.Translate((positions[currentPosition + 1] - (Vector2)transform.position).normalized * timeManager.deltaTime * speed);
                if (isReach(transform.position, positions[currentPosition + 1]))
                {
                        currentPosition++;
                        time = 0;
                }
            }
            else if(currentPosition == positions.Length - 1)
            {
                currentPosition = 0;
                time = 0;
                cycleTime++;
                transform.position = positions[0];
            }
        }
        else
        {
            time += Time.deltaTime;
        }
    }

    public void setPositions(Vector2[] positions)
    {
        this.positions = positions;
        float[] temp = new float[positions.Length];
        for(int i = 0; i < temp.Length; i++)
        {
            temp[i] = 1.0f;
        }
        this.times = temp;
        time = 0;
        cycleTime = 0;
        this.GetComponent<Image>().SetNativeSize();
        transform.position = this.positions[0];
    }

    public void setPositions(Vector2[] positions, int cycle)
    {
        this.positions = positions;
        this.cycle = cycle;
        float[] temp = new float[positions.Length];
        for(int i = 0; i < temp.Length; i++)
        {
            temp[i] = 1.0f;
        }
        this.times = temp;
        time = 0;
        cycleTime = 0;
        this.GetComponent<Image>().SetNativeSize();
        transform.position = this.positions[0];
    }

    public void setPositions(Vector2[] positions, float[] times)
    {
        if (positions.Length == times.Length)
        {
            this.positions = positions;
            this.times = times;
            time = 0;
            cycleTime = 0;
            this.GetComponent<Image>().SetNativeSize();
            transform.position = this.positions[0];
        }
        else
        {
            setPositions(positions);
        }
    }

    public void setPositions(Vector2[] positions, float[] times, int cycle)
    {
        this.cycle = cycle;
        if (positions.Length == times.Length)
        {
            this.positions = positions;
            this.times = times;
            time = 0;
            cycleTime = 0;
            this.GetComponent<Image>().SetNativeSize();
            transform.position = this.positions[0];
        }
        else
        {
            setPositions(positions);
        }
    }

    public bool isReach(Vector2 v1, Vector2 v2)
    {
        if(Vector2.Distance(v1, v2) < 10.0f)
        {
            return true;
        }
        return false;
    }
}
