﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Define
{
	public enum SOUND_MUSIC
	{
		MENU_MUSIC,
		GAME_MUSIC,
		COUNT
	}

	public enum SOUND_EFFECT
	{
		NUMBER_INPUT,
		RESULT_FAIL,
		RESULT_SUCCESS,
		BUTTON_INPUT,
		QUESTION_AUTO_INPUT,
		COUNT_0,
		COUNT_1,
		COUNT_2,
		COUNT_3,
		COUNT_4,
		COUNT_5,
		COUNT_6,
		COUNT_7,
		COUNT_8,
		COUNT_9,

		NULL = 1000,
	}
}

public class SoundManager : MonoBehaviour {

	enum TYPE
	{
		MUSIC,
		SOUND,
		COUNT
	}
	
	[SerializeField]
	AudioSource[] audioSources;

	[SerializeField]
	AudioClip[] clips;

	string musicFileName = Define.SOUND_MUSIC.MENU_MUSIC.ToString();

	static SoundManager Instance = null;

    private void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        GameObject.DontDestroyOnLoad(gameObject);
        Instance = this;
        audioSources = new AudioSource[(int)TYPE.COUNT];
        clips = new AudioClip[(int)Define.SOUND_MUSIC.COUNT];
    }
    public static SoundManager GetInstance()
    {
        return Instance;
    }

	public void PlaySound(Define.SOUND_MUSIC sound)
	{
		if (musicFileName == sound.ToString())
			return;

		if (musicFileName != string.Empty){
			audioSources[(int)TYPE.MUSIC].Stop();
		}

		musicFileName = sound.ToString();

		audioSources[(int)TYPE.MUSIC].clip = clips[(int)sound];
		audioSources[(int)TYPE.MUSIC].loop = true;
		audioSources[(int)TYPE.MUSIC].volume = 1f;
		audioSources[(int)TYPE.MUSIC].Play();
	}

	public void PlaySound(Define.SOUND_EFFECT sound)
	{
		AudioClip clip = (AudioClip)Instantiate(Resources.Load("Sound/Effects/" + sound.ToString()));
        if (clip == null)
        {
            Debug.LogError("오디오 소스 오류- " + sound.ToString());
            return;
        }
        audioSources[(int)TYPE.SOUND].clip = clip;
		audioSources[(int)TYPE.SOUND].loop = false;
		audioSources[(int)TYPE.SOUND].volume = 1f;
		audioSources[(int)TYPE.SOUND].Play();
	}
}
