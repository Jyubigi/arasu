﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class blanks : MonoBehaviour {

    [SerializeField]
    int fill;
    private void Awake()
    {
        fill = 0;
    }
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void setAnswer(dragable answer)
    {
        if (!isFull())
        {
            blank temp = transform.GetChild(fill).GetComponent<blank>();
            if (temp != null)
            {
                if (answer != temp.correct && temp.must)
                {
                    if (temp.GetComponent<AudioSource>() != null)
                    {
                        temp.GetComponent<AudioSource>().PlayOneShot(temp.GetComponent<AudioSource>().clip);
                    }
                }
                else
                {
                    fill++;
                    temp.image = answer.GetComponent<dragable>().dragImage;
                    temp.gameObject.GetComponent<Image>().sprite = temp.image;
                    bool first;
                    if(temp.answer == null)
                    {
                        first = true;
                    }
                    else
                    {
                        first = false;
                    }
                    temp.answer = answer;
                    transform.root.SendMessage("fillBlank", first, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }

    public void clear()
    {
        fill = 0;
    }

    public bool isFull()
    {
        return fill >= transform.childCount;
    }

    public void setSelect(bool isSelect)
    {
        if (!isFull())
        {
            if (isSelect)
            {
                GamesManager.getInstance().setBlank(transform.GetChild(fill).GetComponent<blank>(), true);
            }
            else
            {
                GamesManager.getInstance().setBlank(null, true);
            }
        }
        else
        {
            GamesManager.getInstance().setBlank(null, true);
        }
    }
}
