﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selfMath {
    //최대공약수
    public static int maxCommonFactor(int m, int n)
    {
        int r;
        while (true)
        {
            r = m % n;
            if (r == 0)
            {
                return n;
            }
            else
            {
                m = n;
                n = r;
            }
        }
    }
    //최소공배수
    public static int minCommonMultiple(int m, int n)
    {
        return m * n / maxCommonFactor(m, n);
    }

    //공약수
    public static List<int> commonFactor(int m, int n)
    {
        List<int> r = new List<int>();

        int i = m > n ? m : n;
        
        while(i >= 2)
        {
            if(m % i== 0 && n % i == 0)
            {
                r.Add(i);
            }
            i--;
        }

        r.Sort(delegate (int x, int y) 
        {
            if (x == y) return 0;
            else if (x > y) return 1;
            else return -1;
        });

        return r;
    }

    //공배수
    public static List<int> commonMultiple(int m, int n, int limit = 1000)
    {
        List<int> r = new List<int>();

        int i = m > n ? m : n;

        while (i <= limit)
        {
            if (i % m == 0 && i % n == 0)
            {
                r.Add(i);
            }
            i++;
        }

        r.Sort(delegate (int x, int y)
        {
            if (x == y) return 0;
            else if (x > y) return 1;
            else return -1;
        });

        return r;
    }

    //약수
    public static List<int> getFactor(int x)
    {
        List<int> r = new List<int>();

        for (int i = 1; i <= x; i++)
        {
            if (x % i == 0)
            {
                r.Add(i);
            }
        }

        return r;
    }

    //배수
    public static List<int> getMultiply(int x, int limit = 100)
    {
        List<int> r = new List<int>();
        int temp = x;
        
        while(temp <= limit)
        {
            r.Add(temp);
            temp += x;
        }

        return r;
    }
}
