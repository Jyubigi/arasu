﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class blank : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    public bool must;
    [SerializeField]
    public bool canInput;
    [SerializeField]
    public bool playSound;
    public int inputed;
    GamesManager GM;
    public Touch touch;
    public Sprite image;
    public dragable answer;
    public dragable correct;
    bool changeImage, changeScale;
    float time, speed;
    bool selected;
    Sprite questionMark;

    GameObject manager;
    // Use this for initialization
    private void Awake()
    {
        GM = GamesManager.getInstance();
        changeImage = false;    // 이미지가 바뀌는지 확인
        changeScale = false;    // 이미지의 크기가 다 커졌는지 확인
        time = 0f;      // 이미지 애니메이션 시간 초기화
        speed = 2.0f;   // 이미지 애니메이션 속도 설정
        answer = null;
        selected = false;
        inputed = -1;
        manager = transform.root.gameObject;
        questionMark = GetComponent<Image>().sprite;
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // 실시간으로 이미지가 현재 변경되어 애니메이션이 가동되는지 확인, 가동되면 그에 맞춰서 애니메이션 실행
        if (changeImage)
        {
            Animation();
            time += Time.deltaTime * speed;
        }
    }

    public void setAnswer(dragable answer, bool notice = true)
    {
        bool first;
        int zeroSnd_Index = (int)Define.SOUND_EFFECT.COUNT_0;   // 숫자 이미지 음성을 위한 변수
        int curSnd_Index;                                       // 숫자 이미지 음성을 위한 변수
        if (transform.parent.GetComponent<blanks>() == null)
        {
            if (answer != correct && must)
            {
                if(GetComponent<AudioSource>() != null)
                {
                    GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
                }
            }
            else
            {
                this.image = answer.GetComponent<dragable>().dragImage;
                this.gameObject.GetComponent<Image>().sprite = image;
                if(answer == null)
                {
                    first = true;
                }
                else
                {
                    first = false;
                }
                this.answer = answer;
                changeImage = true;     // 숫자 이미지 변경 애니메이션 시작
                time = 0;               // 숫자 이미지 변경 애니메이션 시간 초기화
                if (playSound)
                {
                    curSnd_Index = zeroSnd_Index + int.Parse(this.image.name);                  // 숫자 이미지 음성 위치 찾기
                    //SoundManager.GetInstance().PlaySound((Define.SOUND_EFFECT)curSnd_Index);    // 숫자 이미지 음성 실행
                }
                if (canInput)
                {
                    inputed = answer.getSelf();
                    if (transform.parent.GetComponent<blankFrame>() != null)
                    {
                        //transform.parent.GetComponent<blankFrame>().checkSelf();
                    }
                }
                if (notice)
                {
                    manager.SendMessage("fillBlank", first, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
        else
        {
            transform.parent.GetComponent<blanks>().setAnswer(answer);
        }
    }
    public void setCorrect(dragable correct)
    {
        this.correct = correct;
    }

    public bool isCorrect()
    {
        return this.correct == this.answer ? true : false;
    }
    public void setMust(bool must)
    {
        this.must = must;
    }

    // 숫자 이미지가 바뀌면 커졌다가 작아지는 애니메이션 추가
    void Animation()
    {
        // 작아짐
        if (changeScale)
        {
            this.transform.localScale -= new Vector3(time * 0.1f, time * 0.1f, 1f);
            if (this.transform.localScale.x < 1.0f)
            {
                changeImage = false;
                changeScale = false;
                this.transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
        // 커짐
        else
        {
            this.transform.localScale += new Vector3(time * 0.1f, time * 0.1f, 1f);
            if (this.transform.localScale.x > 1.5f)
            {
                changeScale = true;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (transform.parent.GetComponent<blanks>() == null)
        {
            if (!selected)
            {
                GM.setBlank(this);
            }
            else
            {
                GM.setBlank(null);
            }
        }
        else
        {
            transform.parent.GetComponent<blanks>().setSelect(!selected);
        }
    }

    public void setSelect(bool isSelect)
    {
        if (transform.parent.GetComponent<blanks>() == null)
        {
            if (isSelect)
            {
                GetComponent<Image>().color = new Color(1, 1, 0);
                selected = true;
            }
            else
            {
                GetComponent<Image>().color = new Color(1, 1, 1);
                selected = false;
            }
        }
        else
        {
            transform.parent.GetComponent<blanks>().setSelect(isSelect);
        }
    }

    public void setSelect(bool isSelect, bool isByBlanks)
    {
        if (transform.parent.GetComponent<blanks>() == null || isByBlanks)
        {
            if (isSelect)
            {
                GetComponent<Image>().color = new Color(1, 1, 0);
                selected = true;
            }
            else
            {
                GetComponent<Image>().color = new Color(1, 1, 1);
                selected = false;
            }
        }
        else
        {
            transform.parent.GetComponent<blanks>().setSelect(isSelect);
        }
    }

        public void setSelectable(bool isSelectable)
    {
        this.enabled = isSelectable;
    }

    public void setNull()
    {
        this.GetComponent<Image>().sprite = Resources.Load<Sprite>("Image/Common/Buttons/question_mark");
        this.answer = null;
    }

    public bool hasAnswer()
    {
        return this.answer == null ? false : true;
    }

    public void resetToOrigin()
    {
        GetComponent<Image>().sprite = questionMark;
        answer = null;
    }

    public void setManager(GameObject manager)
    {
        this.manager = manager;
    }
}
