﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public static class Popup {
	public const int CLOSE = 0;
	public const int EXIT = 1;
	public const int NOTICE = 2;
	public const int ONEBTN = 3;
	public const int TWOBTN = 4;
	public const int OKRESULT = 5;
	public const int FAILRESULT = 6;
	public const int LASTNUMBER = 7;
	public const string input_Empty = "입력된 것이 없습니다.";
	public const string input_Full = "더 이상 입력할 수 없습니다.";
	public const string input_Incorrect = "한글을 정확히 입력해주세요!";
	public const string input_Process = "아직 결정할 수 없습니다! 변환해주세요.";
	public const string input_Form = "입력 양식에 맞춰서 입력해주세요.";
	public const string input_Four = "숫자를 4자리까지 채워 주세요.";
	public const string id_Full = "저장할 수 있는 아이디를 초과하셨습니다.";
	public const string pw_Full = "저장할 수 있는 비밀번호를 초과하셨습니다.";
	public const string id_Create = "아이디를 생성하셨습니다!";
	public const string pw_Create = "비밀번호를 생성하셨습니다!";
}
public class PopupManager : MonoBehaviour {
	private static PopupManager Instance;
	private Transform background, exit, notice, one_Button, two_Button, ok_Result, fail_Result;

	private Transform btn_Yes, btn_No, btn_Left, btn_Right;

	// Use this for initialization
	void Start () {
		if(Instance != null){
			Destroy(this.gameObject);
		}
		else{
			DontDestroyOnLoad(this.gameObject);
			Instance = this;
			this.Init();
			this.InitButton();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		background = this.transform.GetChild(0);
		exit = this.transform.GetChild(Popup.EXIT);
		notice = this.transform.GetChild(Popup.NOTICE);
		one_Button = this.transform.GetChild(Popup.ONEBTN);
		two_Button = this.transform.GetChild(Popup.TWOBTN);
		ok_Result = this.transform.GetChild(Popup.OKRESULT);
		fail_Result = this.transform.GetChild(Popup.FAILRESULT);

		btn_Left = two_Button.GetChild(1).GetChild(0);
		btn_Right = two_Button.GetChild(1).GetChild(1);
		btn_Yes = exit.GetChild(1).GetChild(0);
		btn_No = exit.GetChild(1).GetChild(1);
	}
	private void InitButton(){
		btn_Yes.gameObject.GetComponent<Button>().onClick.AddListener(delegate{ScenesManager.GetInstance().ExitGame();});
		btn_No.gameObject.GetComponent<Button>().onClick.AddListener(delegate{PopupListner(Popup.CLOSE);});
		one_Button.GetChild(1).GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(delegate{PopupListner(Popup.CLOSE);});
		two_Button.GetChild(1).GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(delegate{PopupListner(Popup.CLOSE);});
        fail_Result.GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(delegate { PopupListner(Popup.CLOSE); });
    }

	public static PopupManager GetInstance(){
		return Instance;
	}

	public void PopupListner(int choice, string str = "", string left = "", UnityAction leftAc = null){
		if(choice == Popup.CLOSE){
			this.ClosePopup();
		}
		else if(choice == Popup.EXIT){
			background.gameObject.SetActive(true);
			exit.gameObject.SetActive(true);
		}
		else if(choice == Popup.NOTICE){
			this.NoticePopup(str);
		}
		else if(choice == Popup.ONEBTN){
			this.OnePopup(str);
		}
		else if(choice == Popup.TWOBTN){
			this.TwoPopup(str, left, leftAc);
		}
		else if(choice == Popup.OKRESULT){
			this.OkPopup();
		}
		else if(choice == Popup.FAILRESULT){
			this.FailPopup();
		}
	}

	public void ClosePopup(){
		if(two_Button.gameObject.activeInHierarchy){
			two_Button.GetChild(1).GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
		}
		
		background.gameObject.SetActive(false);
		for(int i = Popup.EXIT; i < Popup.LASTNUMBER; i ++){
			this.transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	private void NoticePopup(string str){
		notice.gameObject.SetActive(true);
		notice.GetChild(0).gameObject.GetComponent<Text>().text = str;
		Invoke("NoticeAnimation", 2);
		Invoke("ClosePopup", 3);
		notice.gameObject.GetComponent<Animator>().SetBool("isExit", false);
	}

	private void OnePopup(string str){
		one_Button.gameObject.SetActive(true);
		one_Button.GetChild(0).gameObject.GetComponent<Text>().text = str;
	}
	
	private void TwoPopup(string str, string leftBtn, UnityAction leftAc){
		two_Button.gameObject.SetActive(true);
		two_Button.GetChild(0).gameObject.GetComponent<Text>().text = str;
		two_Button.GetChild(1).GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = leftBtn;
		two_Button.GetChild(1).GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(leftAc);
	}

	public void OkPopup(){
		ok_Result.gameObject.SetActive(true);
	}

	public void FailPopup(){
		fail_Result.gameObject.SetActive(true);
	}
	private void NoticeAnimation(){
		notice.gameObject.GetComponent<Animator>().SetBool("isExit", true);
	}
}
