﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highLighting : MonoBehaviour {

    [SerializeField]
    int mode = 0;

    static int statCount = 0; // Controll all unit at once

    static List<highLighting> onceInstance = new List<highLighting>(); // Number of units which controll at once

    static bool onceWay = true;

    bool way = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(mode == 0)
        {
            if(way)
            {
                GetComponent<Image>().color = GetComponent<Image>().color - Color.black * 0.01f;
            }
            else
            {
                GetComponent<Image>().color = GetComponent<Image>().color + Color.black * 0.01f;
            }
            if (GetComponent<Image>().color.a < 0.3f || GetComponent<Image>().color.a >= 1.0f)
            {
                way = !way;
            }
        }
        else if(mode == 1)
        {
            if (way)
            {
                GetComponent<Image>().color = GetComponent<Image>().color - (Color.red - Color.black) * 0.01f;
                if (GetComponent<Image>().color.r < 0.7f)
                {
                    way = !way;
                }
            }
            else
            {
                GetComponent<Image>().color = GetComponent<Image>().color + (Color.red - Color.black) * 0.01f;
                if (GetComponent<Image>().color.r >= 0.7f)
                {
                    way = !way;
                }
            }
        }
        else if(mode == 2)
        {
            if (!onceInstance.Contains(this))
            {
                onceInstance.Add(this);
            }
            statCount++;
            if(statCount >= 30 * onceInstance.Count)
            {
                foreach(highLighting temp in onceInstance)
                {
                    Color tempColor = temp.GetComponent<Image>().color;
                    if (onceWay)
                    {
                        temp.GetComponent<Image>().color = new Color(tempColor.r, tempColor.g, tempColor.b, 1);
                    }
                    else
                    {
                        temp.GetComponent<Image>().color = new Color(tempColor.r, tempColor.g, tempColor.b, 0);
                    }
                }
                statCount = 0;
                onceWay = !onceWay;
            }
        }
        else if(mode == 3)
        {
            if (way)
            {
                GameObject white = new GameObject();
                white.AddComponent<RectTransform>();
                white.transform.parent = transform;
                white.transform.localPosition = Vector2.zero;
                white.AddComponent<Image>();
                white.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
                gameObject.GetComponent<Image>().sprite = null;
                gameObject.GetComponent<Image>().color = Color.green;
                white.GetComponent<RectTransform>().sizeDelta = white.transform.parent.GetComponent<RectTransform>().sizeDelta;
                white.transform.localScale = Vector2.one;
                way = false;
            }
        }
	}

    public void setMode(int mode)
    {
        this.mode = mode;
        if(mode == 2)
        {
            onceInstance.Add(this);
        }
    }

    public static void clearOnce()
    {
        foreach(highLighting temp in onceInstance)
        {
            temp.GetComponent<Image>().color += Color.black;
            temp.enabled = false;
        }

        onceInstance.Clear();
    }

    public void clear()
    {
        if (onceInstance.Contains(this))
        {
            GetComponent<Image>().color += Color.black;
            onceInstance.Remove(this);
        }
    }
}
