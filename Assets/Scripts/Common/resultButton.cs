﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class resultButton : MonoBehaviour {

    Sprite[] resultImages;

    private void Awake()
    {
        resultImages = Resources.LoadAll<Sprite>("Image/Common/Result");
    }
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (GetComponentInChildren<jumpFrog>() != null)
        {
            transform.GetComponent<Image>().sprite = resultImages[1];
        }
        else
        {
            transform.GetComponent<Image>().sprite = resultImages[0];
        }
    }
}
