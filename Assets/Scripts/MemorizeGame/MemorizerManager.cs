﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemorizerManager : MonoBehaviour {

    Timer gameTimer;
    [SerializeField]
    List<Sprite> images = new List<Sprite>();
    [SerializeField]
    Sprite[] numImages;
    List<Memorizer_Select> answers = new List<Memorizer_Select>();
    List<Sprite> corrects = new List<Sprite>();
    [SerializeField]
    static int level = 0;
    bool isSelect;
    Transform selects;
    Transform quiz;

    static MemorizerManager instance;

    public static MemorizerManager getInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        foreach(Sprite tempSprite in Resources.LoadAll<Sprite>("Image/Game/Memorizer/")){
            images.Add(tempSprite);
        }
        numImages = Resources.LoadAll<Sprite>("Image/Common/Number/problem/");

        quiz = transform.GetChild(1);
        selects = transform.GetChild(2);

        gameTimer = GetComponentInChildren<Timer>();
        isSelect = false;
    }
    // Use this for initialization
    void Start () {
        setQuizImage();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void setQuizImage()
    {
        List<int> rnd = new List<int>();
        int max;
        switch (level)
        {
            case 0:
                max = 3;
                break;
            case 1:
                max = 5;
                break;
            case 2:
                max = 7;
                break;
            default:
                max = 3;
                break;
        }
        while(rnd.Count != max)
        {
            int x = Random.Range(0, 15);
            if (!rnd.Contains(x))
            {
                rnd.Add(x);
                corrects.Add(images[x]);
                quiz.GetChild(0).GetChild(rnd.Count - 1).GetComponent<Image>().sprite = images[x];
            }
            else
            {

            }
        }
        Invoke("", 10.0f);
    }

    void startMathQuiz()
    {
        isSelect = false;
        transform.GetChild(3).gameObject.SetActive(false);// 가이드 ㅔㄱ스트 없앰
        quiz.GetChild(0).gameObject.SetActive(false);
        
        Transform mathQ = transform.GetChild(1).GetChild(1);
        mathQ.gameObject.SetActive(true);

        int x = Random.Range(5, 25);
        int y = Random.Range(5, 25);

        numberFrame.addNumber(x, Vector2.zero, mathQ.GetChild(0).GetChild(0), numImages);
        numberFrame.addNumber(y, Vector2.zero, mathQ.GetChild(0).GetChild(1), numImages);

        blankFrame.addBlank(x + y, Vector2.zero, mathQ.GetChild(0).GetChild(2), transform.GetChild(4).GetComponentsInChildren<dragable>());
    }

    void startSelect()
    {
        quiz.GetChild(1).gameObject.SetActive(false);
        selects.gameObject.SetActive(true);
        gameTimer.startTimer();
        isSelect = true;
    }

    public void checkAnswer()
    {
        if (isSelect)
        {
            bool isCorrect = true;
            bool preferNum = false;
            List<Sprite> selected = new List<Sprite>();

            foreach(Memorizer_Select tempMS in selects.GetComponentsInChildren<Memorizer_Select>())
            {
                if (tempMS.isSelect())
                {
                    selected.Add(tempMS.GetComponent<Image>().sprite);
                }
            }

            if(selected.Count == corrects.Count)
            {
                preferNum = true;
                foreach (Sprite tempSprite in selected)
                {
                    if (!corrects.Contains(tempSprite))
                    {
                        isCorrect = false;
                    }
                    else
                    {
                        isCorrect &= true;
                    }
                }
            }
            else
            {
                PopupManager.GetInstance().PopupListner(Popup.FAILRESULT);
            }

            if(isCorrect && preferNum)
            {
                highLighting.clearOnce();
                level++;
                gameTimer.pauseTimer();
                PopupManager.GetInstance().PopupListner(Popup.OKRESULT);
            }
            else
            {
                PopupManager.GetInstance().PopupListner(Popup.FAILRESULT);
            }
        }
        else
        {
            bool isCorrect = true;
            bool isExist = false;
            if(quiz.GetChild(1).GetChild(0).GetComponentInChildren<blank>() != null)
            {
                isExist = true;
            }
            if (isExist)
            {
                foreach(blank tempBlank in quiz.GetChild(1).GetChild(0).GetComponentsInChildren<blank>())
                {
                    if (tempBlank.isCorrect())
                    {
                        isCorrect &= tempBlank.isCorrect();
                    }
                    else
                    {
                        isCorrect = false;
                        tempBlank.setNull();
                    }
                }
            }
            if(isExist && isCorrect)
            {
                startSelect();
            }
            else
            {
                PopupManager.GetInstance().PopupListner(Popup.FAILRESULT);
            }
        }
    }

    public void fillBlank()
    {
        checkAnswer();
    }
}
