﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Memorizer_Select : MonoBehaviour, IPointerClickHandler
{
    MemorizerManager MM;

    [SerializeField]
    bool select;

    private void Awake()
    {
        MM = MemorizerManager.getInstance();
        select = false;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (select)
        {
            select = false;
            GetComponent<highLighting>().enabled = false;
            GetComponent<highLighting>().clear();
        }
        else
        {
            select = true;
            GetComponent<highLighting>().enabled = true;
        }
    }

    public bool isSelect()
    {
        return select;
    }
}
