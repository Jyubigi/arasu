﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    Text self;

    float currentTime;

    bool isGo;

    private void Awake()
    {
        self = GetComponent<Text>();
        currentTime = 0.0f;
        isGo = false;
    }
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (isGo)
        {
            currentTime += TimeManager.GetInstance().deltaTime;
            self.text = System.Math.Round(currentTime, 1).ToString();
        }	
	}

    public void startTimer()
    {
        isGo = true;
    }

    public void pauseTimer()
    {
        isGo = false;
    }

    public void resetTimer()
    {
        currentTime = 0.0f;
        self.text = currentTime.ToString();
    }
}
