﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAnimation : MonoBehaviour {
	Transform right;
	GameObject finger;
	Vector2[] position;

	// Use this for initialization
	void Start () {
		position = new Vector2[2];
		right = GameObject.Find("Right").transform;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 사용방법을 알려주기 위한 손가락 움직이는 애니메이션
	public void PlayFinger(Transform destination){
		finger = Instantiate<GameObject>(Resources.Load<GameObject>("Pref/Finger"), right);
		position[0] = new Vector2(right.position.x, right.position.y);
		position[1] = new Vector2(destination.position.x, destination.position.y);
		finger.GetComponent<finger>().setPositions(position, 5);
	}

}
