﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferRegular {

	public string ChangeToDate(string date){
		string sPattern = "^\\d{4}.\\d{2}.\\d{2}$";
		string resultDate = date;

		Debug.Log("입력 받은 생년월일: " + resultDate);
		if(resultDate.Equals("")){
			return "";
		}

		if(System.Text.RegularExpressions.Regex.IsMatch(resultDate, sPattern)){
			Debug.Log("정확히 입력됨.");
			resultDate = resultDate.Substring(2);
			resultDate = resultDate.Replace(".", "");
			return resultDate;
		}
		else {
			Debug.Log("정규식에 맞지 않음.");
			return "-";
		}
	}

	public string ChangeToPhone(string phoneNumber){
		string sPattern = "^\\d{3}-\\d{4}-\\d{4}$";
		string resultPhoneNumber = phoneNumber;

		Debug.Log("입력 받은 전화번호: " + phoneNumber);
		if(resultPhoneNumber.Equals("")){
			return "";
		}

		if(System.Text.RegularExpressions.Regex.IsMatch(phoneNumber, sPattern)){
			Debug.Log("정확히 입력됨.");
			resultPhoneNumber =resultPhoneNumber.Substring(9);
			return resultPhoneNumber;
		}
		else {
			Debug.Log("정규식에 맞지 않음.");
			return "-";
		}
	}
}
