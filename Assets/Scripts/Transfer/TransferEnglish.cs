﻿using UnityEngine;

public class TransferEnglish {
	/* **********************************************
	* 자음 모음 분리
	* 설연수 -> ㅅㅓㄹㅇㅕㄴㅅㅜ,    바보 -> ㅂㅏㅂㅗ
	* **********************************************/
	/** 초성 - 가(ㄱ), 날(ㄴ) 닭(ㄷ) */
	public static char[] arrChoSung = { '\u3131', '\u3132', '\u3134', '\u3137', '\u3138',
			'\u3139', '\u3141', '\u3142', '\u3143', '\u3145', '\u3146', '\u3147', '\u3148',
			'\u3149', '\u314a', '\u314b', '\u314c', '\u314d', '\u314e' };
	/** 중성 - 가(ㅏ), 야(ㅑ), 뺨(ㅑ)*/
	public static char[] arrJungSung = { '\u314f', '\u3150', '\u3151', '\u3152',
			'\u3153', '\u3154', '\u3155', '\u3156', '\u3157', '\u3158', '\u3159', '\u315a',
			'\u315b', '\u315c', '\u315d', '\u315e', '\u315f', '\u3160', '\u3161', '\u3162',
			'\u3163' };
	/** 종성 - 가(없음), 갈(ㄹ) 천(ㄴ) */
	public static char[] arrJongSung = { '\u0000', '\u3131', '\u3132', '\u3133',
			'\u3134', '\u3135', '\u3136', '\u3137', '\u3139', '\u313a', '\u313b', '\u313c',
			'\u313d', '\u313e', '\u313f', '\u3140', '\u3141', '\u3142', '\u3144', '\u3145',
			'\u3146', '\u3147', '\u3148', '\u314a', '\u314b', '\u314c', '\u314d', '\u314e' };
	
	
	/* **********************************************
	 * 암호 알파벳으로 변환
	 * ㄱ = a, ㄴ = b, ㄷ = c, ㄹ = d, ㅁ = e, ㅂ = f, ... , ㅎ = ㅜ
	 * ㅏ = n, ㅑ = o, ㅓ = p, ㅕ = q, ...
	 * 설연수 -> , 멍충 -> ajdcnd 
	 * **********************************************/
	/** 초성 - 가(ㄱ), 날(ㄴ) 닭(ㄷ) */
	public static string[] arrChoSungEnc = { "a", "aa", "b", "c", "cc",
		"d", "e", "f", "ff", "g", "gg", "h", "i",
		"ii", "j", "k", "l", "m", "n" };
	
	/** 중성 - 가(ㅏ), 야(ㅑ), 뺨(ㅑ)*/
	public static string[] arrJungSungEnc = { "o", "ox", "p", "px",
		"q", "qx", "r", "rx", "s", "so", "sox", "sx",
		"t", "u", "uq", "uqx", "ux", "v", "w", "wx",
		"x" };
	
	/** 종성 - 가(없음), 갈(ㄹ) 천(ㄴ) */
	public static string[] arrJongSungEnc = { "", "a", "aa", "ag",
		"b", "bi", "bn", "c", "d", "da", "de", "df",
		"dg", "dl", "dm", "dn", "e", "f", "fg", "g",
		"gg", "h", "i", "j", "k", "l", "m", "n" };
	
	/** 단일 자음 - ㄱ,ㄴ,ㄷ,ㄹ... (ㄸ,ㅃ,ㅉ은 단일자음(초성)으로 쓰이지만 단일자음으론 안쓰임) */
	public static string[] arrSingleJaumEnc = { "a", "aa", "ag",
		"b", "bi", "bn", "c","cc" ,"d", "da", "de", "df",
		"dg", "dl", "dm", "dn", "e", "f","ff", "fg", "g",
		"gg", "h", "i", "ii", "j", "k", "l", "m", "n" };
	
	/* **********************************************
	 * 알파벳으로 변환
	 * 설연수 -> tjfdustn, 멍충 -> ajdcnd 
	 * **********************************************/
	/** 초성 - 가(ㄱ), 날(ㄴ) 닭(ㄷ) */
	public static string[] arrChoSungEng = { "r", "R", "s", "e", "E",
		"f", "a", "q", "Q", "t", "T", "d", "w",
		"W", "c", "z", "x", "v", "g" };
	
	/** 중성 - 가(ㅏ), 야(ㅑ), 뺨(ㅑ)*/
	public static string[] arrJungSungEng = { "k", "o", "i", "O",
		"j", "p", "u", "P", "h", "hk", "ho", "hl",
		"y", "n", "nj", "np", "nl", "b", "m", "ml",
		"l" };
	
	/** 종성 - 가(없음), 갈(ㄹ) 천(ㄴ) */
	public static string[] arrJongSungEng = { "", "r", "R", "rt",
		"s", "sw", "sg", "e", "f", "fr", "fa", "fq",
		"ft", "fx", "fv", "fg", "a", "q", "qt", "t",
		"T", "d", "w", "c", "z", "x", "v", "g" };
	
	/** 단일 자음 - ㄱ,ㄴ,ㄷ,ㄹ... (ㄸ,ㅃ,ㅉ은 단일자음(초성)으로 쓰이지만 단일자음으론 안쓰임) */
	public static string[] arrSingleJaumEng = { "r", "R", "rt",
		"s", "sw", "sg", "e","E" ,"f", "fr", "fa", "fq",
		"ft", "fx", "fv", "fg", "a", "q","Q", "qt", "t",
		"T", "d", "w", "W", "c", "z", "x", "v", "g" };

	public string processTrans;
	public string ChangeToEnglish(string word){
		string result		= "";									// 결과 저장할 변수
		string resultEng	= "";									// 알파벳으로
		
		for (int i = 0; i < word.Length; i++) {
			
			/*  한글자씩 읽어들인다. */
			char chars = (char) (word.ToCharArray()[i] - 0xAC00);

			if (chars >= 0 && chars <= 11172) {
				/* A. 자음과 모음이 합쳐진 글자인경우 */

				/* A-1. 초/중/종성 분리 */
				int chosung 	= chars / (21 * 28);
				int jungsung 	= chars % (21 * 28) / 28;
				int jongsung 	= chars % (21 * 28) % 28;

				
				/* A-2. result에 담기 */
				result = result + arrChoSung[chosung] + arrJungSung[jungsung];

				
				/* 자음분리 */
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					result =  result + arrJongSung[jongsung];
				}

				/* 알파벳으로 */
				resultEng = resultEng + arrChoSungEng[chosung] + arrJungSungEng[jungsung];
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					resultEng =  resultEng + arrJongSungEng[jongsung];
				}

			} else {
				/* B. 한글이 아니거나 자음만 있을경우 */

				/* 자음분리 */
				result = result + ((char)(chars + 0xAC00));
				
				/* 알파벳으로 */
				if( chars>=34097 && chars<=34126){
					/* 단일자음인 경우 */
					int jaum 	= (chars-34097);
					resultEng = resultEng + arrSingleJaumEng[jaum];
				} else if( chars>=34127 && chars<=34147) {
					/* 단일모음인 경우 */
					int moum 	= (chars-34127);
					resultEng = resultEng + arrJungSungEng[moum];
				} else {
					/* 알파벳인 경우 */
					resultEng = resultEng + ((char)(chars + 0xAC00));
				}
			}//if
			
		}//for

		Debug.Log(result);
		Debug.Log(resultEng);
		return resultEng;
	}

	public string ChangeToEncryption(string word){
		string result		= "";									// 결과 저장할 변수
		string resultEnc	= "";									// 알파벳으로
		this.processTrans = "";

		for (int i = 0; i < word.Length; i++) {
			
			/*  한글자씩 읽어들인다. */
			char chars = (char) (word.ToCharArray()[i] - 0xAC00);

			if (chars >= 0 && chars <= 11172) {
				/* A. 자음과 모음이 합쳐진 글자인경우 */

				/* A-1. 초/중/종성 분리 */
				int chosung 	= chars / (21 * 28);
				int jungsung 	= chars % (21 * 28) / 28;
				int jongsung 	= chars % (21 * 28) % 28;

				
				/* A-2. result에 담기 */
				result = result + arrChoSung[chosung] + arrJungSung[jungsung];

				this.processTrans += arrChoSung[chosung] + " = " + arrChoSungEnc[chosung] + ", " + arrJungSung[jungsung] + " = " + arrJungSungEnc[jungsung] + ", ";
				/* 자음분리 */
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					result =  result + arrJongSung[jongsung];
					this.processTrans += arrJongSung[jongsung] + " = " + arrJongSungEnc[jongsung] + ", ";
				}

				/* 알파벳으로 */
				resultEnc = resultEnc + arrChoSungEnc[chosung] + arrJungSungEnc[jungsung];
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					resultEnc =  resultEnc + arrJongSungEnc[jongsung];
				}

			} else {
				/* B. 한글이 아니거나 자음만 있을경우 */

				/* 자음분리 */
				result = result + ((char)(chars + 0xAC00));
				
				/* 알파벳으로 */
				if( chars>=34097 && chars<=34126){
					/* 단일자음인 경우 */
					int jaum 	= (chars-34097);
					resultEnc = resultEnc + arrSingleJaumEnc[jaum];
				} else if( chars>=34127 && chars<=34147) {
					/* 단일모음인 경우 */
					int moum 	= (chars-34127);
					resultEnc = resultEnc + arrJungSungEnc[moum];
				} else {
					/* 알파벳인 경우 */
					resultEnc = resultEnc + ((char)(chars + 0xAC00));
				}
			}//if
			
		}//for

		this.processTrans = this.processTrans.Remove(this.processTrans.Length - 2);
		Debug.Log(result);
		Debug.Log(resultEnc);
		return resultEnc;
	}
}
