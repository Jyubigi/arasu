﻿using UnityEngine;

public class TransferNumber {
	/* **********************************************
	* 자음 모음 분리
	* 설연수 -> ㅅㅓㄹㅇㅕㄴㅅㅜ,    바보 -> ㅂㅏㅂㅗ
	* **********************************************/
	/** 초성 - 가(ㄱ), 날(ㄴ) 닭(ㄷ) */
	public static char[] arrChoSung = { '\u3131', '\u3132', '\u3134', '\u3137', '\u3138',
			'\u3139', '\u3141', '\u3142', '\u3143', '\u3145', '\u3146', '\u3147', '\u3148',
			'\u3149', '\u314a', '\u314b', '\u314c', '\u314d', '\u314e' };
	/** 중성 - 가(ㅏ), 야(ㅑ), 뺨(ㅑ)*/
	public static char[] arrJungSung = { '\u314f', '\u3150', '\u3151', '\u3152',
			'\u3153', '\u3154', '\u3155', '\u3156', '\u3157', '\u3158', '\u3159', '\u315a',
			'\u315b', '\u315c', '\u315d', '\u315e', '\u315f', '\u3160', '\u3161', '\u3162',
			'\u3163' };
	/** 종성 - 가(없음), 갈(ㄹ) 천(ㄴ) */
	public static char[] arrJongSung = { '\u0000', '\u3131', '\u3132', '\u3133',
			'\u3134', '\u3135', '\u3136', '\u3137', '\u3139', '\u313a', '\u313b', '\u313c',
			'\u313d', '\u313e', '\u313f', '\u3140', '\u3141', '\u3142', '\u3144', '\u3145',
			'\u3146', '\u3147', '\u3148', '\u314a', '\u314b', '\u314c', '\u314d', '\u314e' };

	/* **********************************************
	 * 숫자로 변환
	 * 설연수 -> tjfdustn, 멍충 -> ajdcnd 
	 * **********************************************/
	/** 초성 - 가(ㄱ), 날(ㄴ) 닭(ㄷ) */
	public static string[] arrChoSungNum = { "01", "0101", "02", "03", "0303",
		"04", "05", "06", "0606", "07", "0707", "08", "09",
		"0909", "10", "11", "12", "13", "14" };
	
	/** 중성 - 가(ㅏ), 야(ㅑ), 뺨(ㅑ)*/
	public static string[] arrJungSungNum = { "1", "10", "2", "20",
		"3", "30", "4", "40", "5", "51", "510", "50",
		"6", "7", "73", "72", "70", "8", "9", "90",
		"0" };
	
	/** 종성 - 가(없음), 갈(ㄹ) 천(ㄴ) */
	public static string[] arrJongSungNum = { "", "01", "0101", "0107",
		"02", "0209", "0214", "03", "04", "0401", "0405", "0406",
		"0407", "0412", "0413", "0414", "05", "06", "0607", "07",
		"0707", "08", "09", "10", "11", "12", "13", "14" };
	
	/** 단일 자음 - ㄱ,ㄴ,ㄷ,ㄹ... (ㄸ,ㅃ,ㅉ은 단일자음(초성)으로 쓰이지만 단일자음으론 안쓰임) */
	public static string[] arrSingleJaumNum = { "01", "0101", "0107",
		"02", "0209", "0214", "03","0303" ,"04", "0401", "0405", "0406",
		"0407", "0412", "0413", "0414", "05", "06","0606", "0607", "07",
		"0707", "08", "09", "0909", "10", "11", "12", "13", "14" };

	public string processTrans;

	public string ChangeToNumber(string word){
		string result		= "";									// 결과 저장할 변수
		string resultNum	= "";									// 알파벳으로
		this.processTrans = "";

		for (int i = 0; i < word.Length; i++) {
			
			/*  한글자씩 읽어들인다. */
			char chars = (char) (word.ToCharArray()[i] - 0xAC00);

			if (chars >= 0 && chars <= 11172) {
				/* A. 자음과 모음이 합쳐진 글자인경우 */

				/* A-1. 초/중/종성 분리 */
				int chosung 	= chars / (21 * 28);
				int jungsung 	= chars % (21 * 28) / 28;
				int jongsung 	= chars % (21 * 28) % 28;

				
				/* A-2. result에 담기 */
				result = result + arrChoSung[chosung] + arrJungSung[jungsung];

				this.processTrans += arrChoSung[chosung] + " = " + arrChoSungNum[chosung] + ", " + arrJungSung[jungsung] + " = " + arrJungSungNum[jungsung] + ", ";
				
				/* 자음분리 */
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					result =  result + arrJongSung[jongsung];
					this.processTrans += arrJongSung[jongsung] + " = " + arrJongSungNum[jongsung] + ", ";
				}

				/* 숫자로 */
				resultNum = resultNum + arrChoSungNum[chosung] + arrJungSungNum[jungsung];
				if (jongsung != 0x0000) {
					/* A-3. 종성이 존재할경우 result에 담는다 */
					resultNum =  resultNum + arrJongSungNum[jongsung];
				}

			} else {
				/* B. 한글이 아니거나 자음만 있을경우 */

				/* 자음분리 */
				result = result + ((char)(chars + 0xAC00));
				
				/* 알파벳으로 */
				if( chars>=34097 && chars<=34126){
					/* 단일자음인 경우 */
					int jaum 	= (chars-34097);
					resultNum = resultNum + arrSingleJaumNum[jaum];
				} else if( chars>=34127 && chars<=34147) {
					/* 단일모음인 경우 */
					int moum 	= (chars-34127);
					resultNum = resultNum + arrJungSungNum[moum];
				} else {
					/* 알파벳인 경우 */
					resultNum = resultNum + ((char)(chars + 0xAC00));
				}
			}//if
			
		}//for

		this.processTrans = this.processTrans.Remove(this.processTrans.Length - 2);
		Debug.Log(result);
		Debug.Log(resultNum);
		if(!resultNum.Equals("")){
			resultNum = resultNum.Substring(0, 4);
		}
		Debug.Log(resultNum);
		return resultNum;
	}
}
