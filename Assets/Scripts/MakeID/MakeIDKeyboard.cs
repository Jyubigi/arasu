﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeIDKeyboard : MonoBehaviour {
	private Transform input_Name, result;

	private Transform btn_Transfer, btn_Decide, btn_Back;

	private string korea_Name, result_ID;

	TransferEnglish transfer_English;

	// Use this for initialization
	void Start () {
		input_Name = this.transform.GetChild(0);
		result = this.transform.GetChild(1);
		btn_Transfer = this.transform.GetChild(2);
		btn_Decide = this.transform.GetChild(3);
		btn_Back = this.transform.GetChild(4);
		transfer_English = new TransferEnglish();
		this.Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		korea_Name = "";
		result_ID = "";

		btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("transfer"); } );
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("decide"); } );
		btn_Back.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ GameObject canvas = GameObject.Find("Canvas"); canvas.GetComponent<MakeIDManager>().BoardController("Close", false); } );
	}

	private void TransferListner(string type){
		if(type.Equals("transfer")){
			korea_Name = input_Name.gameObject.GetComponent<InputField>().text;
			result_ID = transfer_English.ChangeToEnglish(korea_Name);
			Debug.Log(result_ID);
			if(result_ID.Equals("")){
				PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
				Debug.Log("입력 안했어!");
			}
			else if(result_ID.Equals("-")){
				PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Incorrect);
				Debug.Log("정확한 이름 입력");
			}
			else {
				result.GetChild(0).gameObject.GetComponent<Text>().text = result_ID;
			}
		}
		else if(type.Equals("decide")){
			if(result_ID.Equals("")){
				PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Process);
				Debug.Log("아직 결정할 수 없어요! 변환해주세요");
			}
			else {
				GameObject canvas = GameObject.Find("Canvas");
				canvas.GetComponent<MakeIDManager>().ChangeIDEng("키보드", korea_Name, result_ID);
				canvas.GetComponent<MakeIDManager>().BoardController("Close", false);
			}
		}
	}
}
