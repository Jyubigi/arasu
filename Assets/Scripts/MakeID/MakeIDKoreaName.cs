﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Text;
using System.IO;

public class MakeIDKoreaName : MonoBehaviour {
	private Transform input_Name, transfer, result;
	private Transform btn_Transfer, btn_Decide, btn_Array, btn_Back;

	private Sprite[] img_Alphabet;

	private string korea_Name, english_Name, result_ID;

	private char[] ary_Korea_Name;
	
	NaverAPITransferRoman transfer_Roman;

	// Use this for initialization
	void Start () {
		input_Name = this.transform.GetChild(0);
		transfer = this.transform.GetChild(1);
		result = this.transform.GetChild(2);
		btn_Transfer = this.transform.GetChild(3);
		btn_Decide = this.transform.GetChild(4);
		btn_Array = this.transform.GetChild(5);
		btn_Back = this.transform.GetChild(6);

		img_Alphabet = Resources.LoadAll<Sprite>("Font/Img_Alphabet");

		transfer_Roman = new NaverAPITransferRoman();
		
		this.Init();
		//input_Name.gameObject.GetComponent<InputField>().onEndEdit.OnOnSubmit(delegate{ transfer_Roman.RequestRoman(input_Name.GetComponent<InputField>().text);});
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		korea_Name = "";
		result_ID = "";
		ary_Korea_Name = new char[30];
		btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("transfer");} );
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("decide");} );
		if(ScenesManager.GetInstance().GetCurrentScene().Equals("MakePWInternet")){
			btn_Back.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ GameObject canvas = GameObject.Find("Canvas"); canvas.GetComponent<MakePWManager>().BoardController("Close", false); } );
		}
		else {
			btn_Back.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ GameObject canvas = GameObject.Find("Canvas"); canvas.GetComponent<MakeIDManager>().BoardController("Close", false); } );
		}
	}

	private void TransferListner(string type){
		if(type.Equals("transfer")){
			this.TransferProcess();
		}
		else if(type.Equals("decide")){
			this.DecideProcess();
		}
		else if(type.Equals("nextLevel")){
			btn_Transfer.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
			btn_Transfer.GetChild(0).gameObject.GetComponent<Text>().text = "한번 지움";
			btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("delete");} );
			btn_Decide.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
			btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ TransferListner("finalDecide");} );
		}
		else if(type.Equals("delete")){
			if(result_ID.Length == 0){
				PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
				Debug.Log("입력된 것이 없습니다.");
			}
			else {
				result_ID = result_ID.Substring(0, result_ID.Length - 1);
				Debug.Log(result_ID);
				result.GetChild(0).gameObject.GetComponent<Text>().text = result_ID;
			}
		}
		else if(type.Equals("finalDecide")){
			if(result_ID.Length == 0){
				PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
				Debug.Log("입력된 것이 없습니다.");
			}
			else {
				if(ScenesManager.GetInstance().GetCurrentScene().Equals("MakeID")){
					GameObject canvas = GameObject.Find("Canvas");
					canvas.GetComponent<MakeIDManager>().ChangeIDEng("한글", korea_Name, result_ID);
					canvas.GetComponent<MakeIDManager>().BoardController("Close", false);
				}
				else if(ScenesManager.GetInstance().GetCurrentScene().Equals("MakePWInternet")){
					GameObject canvas = GameObject.Find("Canvas");
					canvas.GetComponent<MakePWManager>().ChangePWInputEng(korea_Name, result_ID);
					canvas.GetComponent<MakePWManager>().BoardController("Close", false);
				}
			}
		}
	}

	private void AlphabetListner(int index){
		string sum_String = Encoding.ASCII.GetString(BitConverter.GetBytes(index + 97));
		
		Debug.Log(index);
		
		this.ChangeResultID(sum_String.ToCharArray()[0]);
	}

	private void ChangeResultID(char sum_String){
		Debug.Log(sum_String);
		if(result_ID.Length > 9){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Full);
			Debug.Log("꽉찼습니다.");
		}
		else{
			result_ID += sum_String;
			Debug.Log(result_ID);
			this.result.GetChild(0).gameObject.GetComponent<Text>().text = result_ID;
		}
	}

	private void TransferProcess(){
		korea_Name = input_Name.gameObject.GetComponent<InputField>().text;
		english_Name = transfer_Roman.RequestRoman(korea_Name);
		if(english_Name.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else if(english_Name.Equals("-")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Incorrect);
			Debug.Log("정확한 이름 입력");
		}
		else {
			transfer.GetChild(0).gameObject.GetComponent<Text>().text = english_Name;
		}
	}

	private void DecideProcess(){
		if(english_Name.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Process);
			Debug.Log("아직 결정할 수 없어요! 변환해주세요");
		}
		else{
			this.ChangeNameToArray();
			this.TransferListner("nextLevel");
		}
	}
	
	private void ChangeNameToArray(){
		english_Name = english_Name.Replace(" ", "");
		english_Name = english_Name.ToLower();
		ary_Korea_Name = english_Name.ToCharArray();
		btn_Array.gameObject.SetActive(true);
		transfer.gameObject.SetActive(false);
		
		for(int i = 0; i < ary_Korea_Name.Length; i ++){
			Debug.Log(ary_Korea_Name[i]);
			int alphabet_Number = ary_Korea_Name[i] % 97;
			Debug.Log(alphabet_Number);
			this.SettingsAlphabet(alphabet_Number);
		}
	} 

	private void SettingsAlphabet(int index){
		GameObject alphabet = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Btn_Alphabet"), btn_Array);

		ChangeImage.ChangeToImage(alphabet, img_Alphabet[index]);
		alphabet.GetComponent<Button>().onClick.AddListener( delegate{ AlphabetListner(index); } );
	}
}
