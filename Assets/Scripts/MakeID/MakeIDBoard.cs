﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeIDBoard : MonoBehaviour {
	private Transform koreaName;
	private const int KR_NAME = 1;
	private const int KEYBOARD = 2;
	private const int ENCRYPTION = 3;
	private const int FAVORITE = 4;
	private const int BIRTH = 5;
	private const int PHONE = 6;

	// Use this for initialization
	void Start () {
		koreaName = this.transform.GetChild(KR_NAME);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		
	}
	public void BoardListner(string board){
		if(board.Equals("KoreaName")){
			this.ActiveBoard(KR_NAME, true);
		}
		else if(board.Equals("Keyboard")){
			this.ActiveBoard(KEYBOARD, true);
		}
		else if(board.Equals("Encryption")){
			this.ActiveBoard(ENCRYPTION, true);
		}
		else if(board.Equals("Favorite")){
			this.ActiveBoard(FAVORITE, true);
		}
		else if(board.Equals("Birth")){
			this.ActiveBoard(BIRTH, true);
		}
		else if(board.Equals("Phone")){
			this.ActiveBoard(PHONE, true);
		}
		else if(board.Equals("Close")){
			for(int index = 1; index < this.transform.childCount; index ++){
				this.ActiveBoard(index, false);
			}
		}
	}

	private void ActiveBoard(int index, bool active){
		this.transform.GetChild(index).gameObject.SetActive(active);
	}
}
