﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeIDNumber : MonoBehaviour {
	private Transform input_Number, result;
	private Transform btn_Decide, btn_Transfer, btn_Back;

	private TransferRegular transferRegular;

	private string result_Number;

	// Use this for initialization
	void Start () {
		Debug.Log(this.gameObject.name);
		result_Number = "";
		if(this.gameObject.name.Equals("Favorite_Number")){
			input_Number = this.transform.GetChild(0);
			btn_Decide = this.transform.GetChild(1);
			btn_Back = this.transform.GetChild(2);
			this.InitFavorite();
		}
		else if(this.gameObject.name.Equals("Birth_Number")){
			input_Number = this.transform.GetChild(0);
			result = this.transform.GetChild(1);
			btn_Transfer = this.transform.GetChild(2);
			btn_Decide = this.transform.GetChild(3);
			btn_Back = this.transform.GetChild(4);
			transferRegular = new TransferRegular();
			this.InitBirth();
		}
		else if(this.gameObject.name.Equals("Phone_Number")){
			input_Number = this.transform.GetChild(0);
			result = this.transform.GetChild(1);
			btn_Transfer = this.transform.GetChild(2);
			btn_Decide = this.transform.GetChild(3);
			btn_Back = this.transform.GetChild(4);
			transferRegular = new TransferRegular();
			this.InitPhone();
		}
		btn_Back.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ GameObject canvas = GameObject.Find("Canvas"); canvas.GetComponent<MakeIDManager>().BoardController("Close", false); } );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void InitFavorite(){
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Favorite"); } );
	}

	private void InitBirth(){
		btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Birth_Transfer"); } );
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Decide");});
	}

	private void InitPhone(){
		btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Phone_Transfer");} );
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Decide");} );
	}

	private void ButtonListner(string type){
		if(type.Equals("Favorite")){
			this.FavoriteNumberProcess();
		}
		else if(type.Equals("Birth_Transfer")){
			this.BirthNumberProcess();
		}
		else if(type.Equals("Phone_Transfer")){
			this.PhoneNumberProcess();
		}
		else if(type.Equals("Decide")){
			this.NumberEnd();
		}
	}

	private void FavoriteNumberProcess(){
		result_Number = input_Number.gameObject.GetComponent<InputField>().text;
		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else{
			GameObject canvas = GameObject.Find("Canvas");
			canvas.GetComponent<MakeIDManager>().ChangeIDNum(result_Number);
			canvas.GetComponent<MakeIDManager>().BoardController("Close", false);
		}
	}

	private void BirthNumberProcess(){
		string date = input_Number.gameObject.GetComponent<InputField>().text;
		result_Number = transferRegular.ChangeToDate(date);

		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else if(result_Number.Equals("-")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Form);
			Debug.Log("양식에 맞춰서 입력해주세요!");
		}
		else{
			result.GetChild(0).gameObject.GetComponent<Text>().text = result_Number;
		}
		Debug.Log(result_Number);
	}

	private void NumberEnd(){
		if(result_Number.Equals("") || result_Number.Equals("-")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Process);
			Debug.Log("아직 결정할 수 없어요! 변환해주세요");
		}
		else {
			GameObject canvas = GameObject.Find("Canvas");
			canvas.GetComponent<MakeIDManager>().ChangeIDNum(result_Number);
			canvas.GetComponent<MakeIDManager>().BoardController("Close", false);
		}
	}

	private void PhoneNumberProcess(){
		string phoneNumber = input_Number.gameObject.GetComponent<InputField>().text;
		result_Number = transferRegular.ChangeToPhone(phoneNumber);

		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else if(result_Number.Equals("-")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Form);
			Debug.Log("양식에 맞춰서 입력해주세요!");
		}
		else{
			result.GetChild(0).gameObject.GetComponent<Text>().text = result_Number;
		}
		Debug.Log(result_Number);
	}
}