﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeIDManager : MonoBehaviour {
	private Transform id_Field, buttons, board;
	private Transform btn_ID_Eng, btn_ID_Num, btn_Decide, btn_ID_Eng_Text, btn_ID_Num_Text;
	
	// Use this for initialization
	private const int ID_ENG = 1;	// ID 영문 입력 Flag
	private const int ID_NUM = 2;	// ID 숫자 입력 Flag

	private string type, id_Name, id_Change;
	private bool check;

	private void Start () {
		if(PlayerManager.GetInstance().IsFullID()){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.id_Full);
			ScenesManager.GetInstance().SceneChange("IDPWMain");
		}
		
		id_Field = this.transform.GetChild(3);
		buttons = this.transform.GetChild(4);
		board = this.transform.GetChild(5);
		btn_ID_Eng = id_Field.GetChild(0);
		btn_ID_Num = id_Field.GetChild(1);
		btn_ID_Eng_Text = btn_ID_Eng.GetChild(0);
		btn_ID_Num_Text = btn_ID_Num.GetChild(0);

		this.Init();
		this.InitButton();
	}
	
	// Update is called once per frame
	void Update () {
		if(!check){
			if(!btn_ID_Eng_Text.GetComponent<Text>().text.Equals("Click!")
			&& !btn_ID_Num_Text.GetComponent<Text>().text.Equals("Click!")){
				this.id_Change = btn_ID_Eng_Text.GetComponent<Text>().text + btn_ID_Num_Text.GetComponent<Text>().text;
				this.check = true;
			}
		}
	}

	private void Init(){
		this.check = false;
		this.type = "";
		this.id_Name = "";
		this.id_Change = "";
		btn_ID_Eng.gameObject.GetComponent<Button>().onClick.AddListener(delegate{ ButtonListner("Eng"); PlayButton();});
		btn_ID_Num.gameObject.GetComponent<Button>().onClick.AddListener(delegate{ ButtonListner("Num"); PlayButton();});
	}

	private void InitButton(){
		buttons.GetChild(ID_ENG).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("KoreaName"); PlayButton();});
		buttons.GetChild(ID_ENG).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Keyboard"); PlayButton();});
		buttons.GetChild(ID_ENG).GetChild(2).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Encryption"); PlayButton();});
		buttons.GetChild(ID_NUM).GetChild(0).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Favorite"); PlayButton();});
		buttons.GetChild(ID_NUM).GetChild(1).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Birth"); PlayButton();});
		buttons.GetChild(ID_NUM).GetChild(2).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Phone"); PlayButton();});
		btn_Decide = buttons.GetChild(0);
		btn_Decide.GetComponent<Button>().onClick.AddListener(delegate{ FinalDecide(); PlayButton();});
	}
	
	// 버튼 Listner
	void ButtonListner(string type){
		if(type.Equals("Eng")){
			this.ActiveButton(ID_ENG, true);
			this.ActiveButton(ID_NUM, false);
		}
		else if(type.Equals("Num")) {
			this.ActiveButton(ID_NUM, true);
			this.ActiveButton(ID_ENG, false);
		}
		else if(type.Equals("Close")){
			this.ActiveButton(ID_NUM, false);
			this.ActiveButton(ID_ENG, false);
		}
	}

	// 버튼의 Active 상태 변경 함수
	private void ActiveButton(int index, bool active){
		buttons.GetChild(index).gameObject.SetActive(active);
		// for(int open = index; open < index + 3; open ++){
		// 	buttons.GetChild(open).gameObject.SetActive(active);
		// }
	}

	public void BoardController(string boardName, bool active = true){
		board.gameObject.SetActive(active);
		board.GetComponent<MakeIDBoard>().BoardListner(boardName);
	}

	public void ChangeIDEng(string type, string name, string changeID){
		this.type = type;
		this.id_Name = name;
		btn_ID_Eng_Text.GetComponent<Text>().text = changeID;
		this.ButtonListner("Close");
	}

	public void ChangeIDNum(string changeNum){
		btn_ID_Num_Text.GetComponent<Text>().text = changeNum;
		this.ButtonListner("Close");
	}

	private void FinalDecide(){
		if(id_Change.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
		}
		else {
			PlayerManager.GetInstance().SetIDAll(type, id_Name, id_Change);
			ScenesManager.GetInstance().SceneChange("IDPWMain");
		}
	}
	
	private void PlayButton(){
		SoundManager.GetInstance().PlaySound(Define.SOUND_EFFECT.BUTTON_INPUT);
	}
}
