﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePWBoard : MonoBehaviour {
	private const int FAVORITE = 1;
	private const int ENCRYPTION= 2;
	private const int INTERNET_KOREANAME = 1;
	private const int INTERNET_SPECIAL = 2;
	private const int INTERNET_FAVORITE = 3;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		
	}
	public void BoardListner(string board){
		if(board.Equals("Favorite")){
			this.ActiveBoard(FAVORITE, true);
		}
		else if(board.Equals("Encryption")){
			this.ActiveBoard(ENCRYPTION, true);
		}
		else if(board.Equals("Transfer_English")){
			this.ActiveBoard(INTERNET_KOREANAME, true);
		}
		else if(board.Equals("Transfer_Special")){
			this.ActiveBoard(INTERNET_SPECIAL, true);
		}
		else if(board.Equals("Transfer_Number")){
			this.ActiveBoard(INTERNET_FAVORITE, true);
		}
		else if(board.Equals("Close")){
			for(int index = 1; index < this.transform.childCount; index ++){
				this.ActiveBoard(index, false);
			}
		}
	}

	private void ActiveBoard(int index, bool active){
		this.transform.GetChild(index).gameObject.SetActive(active);
	}
}
