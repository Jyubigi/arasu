﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakePWSpecial : MonoBehaviour {
	private Transform btn_List;
	private Transform btn_Swung, btn_Exc, btn_At, btn_Number, btn_Dollar, btn_Percent;

	private string korea_Name, result_English;
	
	// Use this for initialization
	void Start () {
		btn_List = this.transform.GetChild(0);

		btn_Swung = btn_List.GetChild(0);
		btn_Exc = btn_List.GetChild(1);
		btn_At = btn_List.GetChild(2);
		btn_Number = btn_List.GetChild(3);
		btn_Dollar = btn_List.GetChild(4);
		btn_Percent = btn_List.GetChild(5);

		this.Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Init(){
		btn_Swung.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Swung");} );
		btn_Exc.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Exc");} );
		btn_At.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("At");} );
		btn_Number.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Number");} );
		btn_Dollar.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Dollar");} );
		btn_Percent.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Percent");} );
	}

	private void ButtonListner(string type){
		if(type.Equals("Swung")){
			this.ChangeSpecial("~");
		}
		else if(type.Equals("Exc")){
			this.ChangeSpecial("!");
		}
		else if(type.Equals("At")){
			this.ChangeSpecial("@");
		}
		else if(type.Equals("Number")){
			this.ChangeSpecial("#");
		}
		else if(type.Equals("Dollar")){
			this.ChangeSpecial("$");
		}
		else if(type.Equals("Percent")){
			this.ChangeSpecial("%");
		}
	}

	private void ChangeSpecial(string special){
		GameObject canvas = GameObject.Find("Canvas");
		canvas.GetComponent<MakePWManager>().ChangePWInputSpe(special);
		canvas.GetComponent<MakePWManager>().BoardController("Close", false);
	}
}
