﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakePWManager : MonoBehaviour {

	private Transform pw_Field, buttons, board;
	private Transform btn_PW_Input, btn_PW_InputEng, btn_PW_InputSpe, btn_PW_InputNum, btn_Decide;
	private Transform btn_PW_InputEng_Text, btn_PW_InputNum_Text, btn_PW_InputSpe_Text;
	
	// Use this for initialization
	private const int PW_MAIN = 0;
	private const int PW_BANK = 2;	// ID 영문 입력 Flag
	
	private string type, pw_Origin, pw_Change;

	private bool check;

	private void Start () {
		pw_Field = this.transform.GetChild(3);
		buttons = this.transform.GetChild(4);
		board = this.transform.GetChild(5);
		type = "";
		pw_Origin = "";
		pw_Change = "";
		if(pw_Field.childCount > 2){
			btn_PW_InputEng = pw_Field.GetChild(0);
			btn_PW_InputNum = pw_Field.GetChild(1);
			btn_PW_InputSpe = pw_Field.GetChild(2);
			btn_PW_InputEng_Text = btn_PW_InputEng.GetChild(0);
			btn_PW_InputNum_Text = btn_PW_InputNum.GetChild(0);
			btn_PW_InputSpe_Text = btn_PW_InputSpe.GetChild(0);
			btn_Decide = buttons.GetChild(0);
			this.InitInternet();
		}
		else {
			this.type = "은행";
			btn_PW_Input = pw_Field.GetChild(0);
			btn_Decide = buttons.GetChild(2);
		
			this.InitBank();
		}
		btn_Decide.GetComponent<Button>().onClick.AddListener(delegate{ FinalDecide(); PlayButton();});
	}
	
	// Update is called once per frame
	void Update () {
		if(pw_Field.childCount > 2 && !check){
			if(!btn_PW_InputEng_Text.GetComponent<Text>().text.Equals("Click!")
			&& !btn_PW_InputSpe_Text.GetComponent<Text>().text.Equals("Click!")
			&& !btn_PW_InputNum_Text.GetComponent<Text>().text.Equals("Click!")){
				this.type = "인터넷";
				this.pw_Origin += btn_PW_InputNum_Text.GetComponent<Text>().text + btn_PW_InputSpe_Text.GetComponent<Text>().text;
				this.pw_Change = btn_PW_InputEng_Text.GetComponent<Text>().text + btn_PW_InputNum_Text.GetComponent<Text>().text + btn_PW_InputSpe_Text.GetComponent<Text>().text;
				check = true;
			}
		}
	}

	private void InitBank(){
		buttons.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Favorite"); PlayButton();});
		buttons.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Encryption"); PlayButton();});
		
	}

	private void InitInternet(){
		check = false;
		btn_PW_InputEng.GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Transfer_English"); PlayButton();});
		btn_PW_InputSpe.GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Transfer_Special"); PlayButton();});
		btn_PW_InputNum.GetComponent<Button>().onClick.AddListener(delegate{ BoardController("Transfer_Number"); PlayButton();});
	}
	
	// 버튼의 Active 상태 변경 함수
	private void ActiveButton(int index, bool active){
		for(int open = index; open < index + 2; open ++){
			buttons.GetChild(open).gameObject.SetActive(active);
		}
	}

	// Board 조작
	public void BoardController(string boardName, bool active = true){
		board.gameObject.SetActive(active);
		board.GetComponent<MakePWBoard>().BoardListner(boardName);
	}

	public void ChangePWInput(string origin, string changePW){
		this.pw_Origin = origin;
		this.pw_Change = changePW;
		btn_PW_Input.GetChild(0).GetComponent<Text>().text = changePW;
	}

	public void ChangePWInputEng(string origin, string changePW){
		this.pw_Origin = origin;
		btn_PW_InputEng_Text.GetComponent<Text>().text = changePW;
	}

	public void ChangePWInputSpe(string changePW){
		btn_PW_InputSpe_Text.GetComponent<Text>().text = changePW;
	}

	public void ChangePWInputNum(string changePW){
		btn_PW_InputNum_Text.GetComponent<Text>().text = changePW;
	}

	private void FinalDecide(){
		if(pw_Change.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
		}
		else {
			PlayerManager.GetInstance().SetPWAll(type, pw_Origin, pw_Change);
			ScenesManager.GetInstance().SceneChange("PasswordMain");
		}
	}

	private void PlayButton(){
		SoundManager.GetInstance().PlaySound(Define.SOUND_EFFECT.BUTTON_INPUT);
	}

}
