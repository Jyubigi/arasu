﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPWManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(PlayerManager.GetInstance().IsFullPW()){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.pw_Full);
			ScenesManager.GetInstance().SceneChange("IDPWMain");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
