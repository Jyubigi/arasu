﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakePWBank : MonoBehaviour {
	private Transform input_Number, result, notice;
	private Transform btn_Decide, btn_Transfer, btn_Back;

	private string result_Number, korea_Name;

	private TransferNumber transferNumber;

	// Use this for initialization
	void Start () {
		Debug.Log(this.gameObject.name);
		result_Number = "";
		if(this.gameObject.name.Equals("Favorite_Number")){
			input_Number = this.transform.GetChild(0);
			btn_Decide = this.transform.GetChild(1);
			btn_Back = this.transform.GetChild(2);
			this.InitFavorite();
		}
		else if(this.gameObject.name.Equals("Encryption")){
			input_Number = this.transform.GetChild(0);
			result = this.transform.GetChild(1);
			btn_Transfer = this.transform.GetChild(2);
			btn_Decide = this.transform.GetChild(3);
			notice = this.transform.GetChild(4);
			btn_Back = this.transform.GetChild(5);
			transferNumber = new TransferNumber();
			this.InitEncryption();
		}
		btn_Back.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ GameObject canvas = GameObject.Find("Canvas"); canvas.GetComponent<MakePWManager>().BoardController("Close", false); } );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void InitFavorite(){
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Favorite"); } );
	}

	private void InitEncryption(){
		btn_Transfer.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Encryption_Transfer"); } );
		btn_Decide.gameObject.GetComponent<Button>().onClick.AddListener( delegate{ ButtonListner("Decide");});
	}

	private void ButtonListner(string type){
		if(type.Equals("Favorite")){
			this.FavoriteNumberProcess();
		}
		else if(type.Equals("Encryption_Transfer")){
			this.EncryptionProcess();
		}
		else if(type.Equals("Decide")){
			this.NumberEnd();
		}
	}

	private void FavoriteNumberProcess(){
		result_Number = input_Number.gameObject.GetComponent<InputField>().text;
		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else{
			GameObject canvas = GameObject.Find("Canvas");
			if(ScenesManager.GetInstance().GetCurrentScene().Equals("MakePWBank")){
				if(result_Number.Length == 4){
					canvas.GetComponent<MakePWManager>().ChangePWInput(result_Number, result_Number);
				}
				else {
					PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Four);
					return;
				}
			}
			else if(ScenesManager.GetInstance().GetCurrentScene().Equals("MakePWInternet")){
				canvas.GetComponent<MakePWManager>().ChangePWInputNum(result_Number);
			}
			canvas.GetComponent<MakePWManager>().BoardController("Close", false);
		}
	}

	private void EncryptionProcess(){
		korea_Name = input_Number.gameObject.GetComponent<InputField>().text;
		result_Number = transferNumber.ChangeToNumber(korea_Name);

		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Empty);
			Debug.Log("입력 안했어!");
		}
		else if(result_Number.Equals("-")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Form);
			Debug.Log("양식에 맞춰서 입력해주세요!");
		}
		else{
			result.GetChild(0).gameObject.GetComponent<Text>().text = result_Number;
			notice.GetChild(0).gameObject.GetComponent<Text>().text = transferNumber.processTrans;
		}
	}

	private void NumberEnd(){
		if(result_Number.Equals("")){
			PopupManager.GetInstance().PopupListner(Popup.NOTICE, Popup.input_Process);
			Debug.Log("아직 결정할 수 없어요! 변환해주세요");
		}
		else {
			GameObject canvas = GameObject.Find("Canvas");
			canvas.GetComponent<MakePWManager>().ChangePWInput(korea_Name, result_Number);
			canvas.GetComponent<MakePWManager>().BoardController("Close", false);
		}
	}
}
