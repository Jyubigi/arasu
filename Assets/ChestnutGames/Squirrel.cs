﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 다람쥐 캐릭터 애니메이션 및 좌표이동을 관리해주는 클래스입니다.
/// </summary>
public class Squirrel : MonoBehaviour
{
    private Animator ani;
    private Vector3 startPos; // 시작좌표
    private Vector3 destPos; //목적지 좌표
    private bool isMove = false;
    private float mtime = 0f;

    private void Awake()
    {
        ani = transform.GetComponent<Animator>();
        startPos = transform.position;
    }
    public void Go(Vector3 _destPos)
    {
        isMove = true;
        mtime = 0f;
        startPos = transform.position;
        destPos = _destPos;
        ani.SetTrigger("SquirrelRun");
    }
    public void Back()
    {
        isMove = true;
        mtime = 0f;
        destPos = startPos;
        startPos = transform.position;
        ani.SetTrigger("SquirrelRun");
    }
    public void Jump()
    {
        ani.SetTrigger("SquirrelJump");
    }
    private void Update()
    {
        if(mtime>1f)
        {
            isMove = false;
            mtime = 0f;
            ani.SetTrigger("SquirrelIdle");
        }
        if(isMove)
        {
            transform.position = Vector3.Slerp(startPos, destPos, mtime);
        }
        mtime += Time.deltaTime;
    }
}