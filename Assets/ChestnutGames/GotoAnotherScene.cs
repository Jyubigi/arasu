﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotoAnotherScene : MonoBehaviour {
    public void Goto(int _sceneNumber)
    {
        Application.LoadLevel(_sceneNumber);
    }
}
