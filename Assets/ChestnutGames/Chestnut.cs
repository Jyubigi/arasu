﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 덧셈, 뼬셈 게임에 사용되는 나무열매 오브젝트의 움직임을 처리하는 클래스입니다.
/// </summary>
public class Chestnut : MonoBehaviour {
    private Vector3 startPos; // 시작좌표
    private Vector3 destPos; //목적지 좌표
    private bool isSway = false;
    private bool isMove =false;
    private bool isPop = false;
    private bool isPush = false;
    private float mTime=  0f; // lerp에 사용할 time값;( 0~1f)
    private float speed = 1f;

    public float SwayScale = 49f; // 얼마나 rotate할지? 제곱값임
    public float PopScale = 2f;
    private Vector3 swayDrection = Vector3.forward;
    private Vector3 nomalScale;

    void Start () {
        nomalScale = transform.localScale;
        startPos = transform.position;
    }
    /// none : 나무열매의 동작을 멈춥니다.
    public void goNone()
    {
        isMove = false;
        isSway = false;
    }
    ///나무열매를 흔듭니다.
    public void goSway()
    {
            isSway = true;
    }
    public void goPop()
    {
        Choi.SoundManager.I.PlaySFX("BUTTON_INPUT");
        isPop = true;
        mTime = 0f;
        speed = 2f;
    }
    /// downY만큼 좌표를 내립니다.
    public void goDown(float _downY)
    {
        isMove = true;
        mTime = 0f;
        startPos = transform.position;
        destPos = transform.position + Vector3.down * _downY;
    }
    // 지정 좌표에 일렬로 둡니다.
    public void goByLine(Vector3 _datumPos,int _index, float _arrspacex = 1f)
    {
        mTime = 0f;
        isMove = true;
        startPos = transform.position;
        destPos = _datumPos  + Vector3.right * _index * _arrspacex;
    }
    //부모 오브젝트를 지정 오브젝트로 바꾸고 해당 오브젝트 좌표에 나란히 둡니다.
    public void goDraw(Transform _parent,int _index, int _arrow =2, float _arrSpacex = 1f, float _arrSpacey= 1f)
    {
        mTime = 0f;
        isMove = true;
        transform.parent = _parent;
        startPos = transform.position;
        destPos = transform.parent.position + Vector3.down * (_index % _arrow) * _arrSpacey + Vector3.right * (_index / _arrow) * _arrSpacex;
    }
    // 지정 좌표에 나란히 둡니다.
    public void goArrange(Vector3 _datumPos, int _index, int _arrrow =2)
    {
        mTime = 0f;
        startPos = transform.position;
        destPos = _datumPos + Vector3.down * (_index / _arrrow) + Vector3.right * (_index % _arrrow);
    }
	// Update is called once per frame
	void Update () {
        if(mTime > 1f)
        {
            if(isPop)
            {
                isPop = false;
                isPush = true;
            }
            else
            {
                isPush = false;
                isMove = false;
                speed = 1f;
            }
            
            mTime = 0f;
        }
        if(isMove)
        {
            transform.position = Vector3.Lerp(startPos, destPos, mTime);
        }
        if(isSway)
        {
            transform.Rotate(swayDrection * Time.deltaTime*SwayScale*0.3f);
            if (transform.eulerAngles.z * transform.eulerAngles.z > SwayScale)
                swayDrection = swayDrection * -1f;
        }
        if(isPop)
        {
            transform.localScale = Vector3.Slerp(nomalScale, nomalScale * PopScale, mTime);
        }
        else if(isPush)
        {
            transform.localScale = Vector3.Lerp(nomalScale *PopScale,nomalScale, mTime);
        }
        mTime += Time.deltaTime*speed;
    }
}
