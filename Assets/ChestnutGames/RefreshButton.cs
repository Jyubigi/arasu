﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshButton : MonoBehaviour {
    public GotoAnotherScene Go;
    public void OnClick()
    {
        Go.Goto(Application.loadedLevel);
    }
}
