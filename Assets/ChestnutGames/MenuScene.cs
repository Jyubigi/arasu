﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScene : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        PopupManager.GetInstance().ClosePopup();
        Choi.SoundManager.I.ChangeBGM("MENU_MUSIC");
    }
}
