﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberAnimation : MonoBehaviour {
    bool isMove = false;
    Vector3 directionPos;
    Vector3 startPos;
    float speed;
    float mtime;
    public void StartAni(Vector3 _directionPos, float _speed = 2f)
    {
        startPos = transform.position;
        speed = _speed;
        directionPos = _directionPos;
        isMove = true;
        mtime = 0f;
        StartCoroutine("Ani");
    }
    IEnumerator Ani()
    {
        while(mtime < 1f)
        {
            mtime += Time.deltaTime;
            transform.position = Vector3.Slerp(startPos, directionPos, mtime);
            yield return null;
        }
        transform.localScale = Vector3.zero;
        yield return null;
    }
}
