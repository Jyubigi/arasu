﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 다람쥐 오브젝트에 붙인다는 가정 하에 만들었습니다.
/// 덧셈뼬샘 게임의 관리자 클래스의 루트클래스입니다.
/// </summary>
public class ChestnutManager : MonoBehaviour {
    protected int sum;//정답.
    protected CoinCounter counter;//숫자 말풍선
    protected Squirrel squirrel;
    protected Sprite[] numImages; //숫자이미지
    protected Transform rightUi;// 오른쪽 ui
    protected Transform mathQ; // 수식부분
    protected GameObject resOb =null;

    public float ArrSpaceX = 0.5f;
    public float ArrSpaceY = 0.3f;
    public int ArrRow = 2;
    /// <summary>
    /// 씬의 오브젝트들을 Find해 변수에 넣어줍니다.
    /// </summary>
    protected void InitScene()
    {
        PopupManager.GetInstance().ClosePopup();
        squirrel = transform.GetComponent<Squirrel>();
        counter = transform.GetChild(0).GetComponent<CoinCounter>();
        numImages = Resources.LoadAll<Sprite>("Image/Common/Number/problem/");
        rightUi = GameObject.Find("Right").transform;
        mathQ = GameObject.Find("MathQuiz").transform;
    }
    public void CheckResult()
    {
        if (resOb == null)
        {
        }
        else
        {
            if(resOb.GetComponent<blankFrame>().getSelf() == sum)
            {
                PopupManager.GetInstance().OkPopup();
            }
            else
            {
                PopupManager.GetInstance().FailPopup();
            }
           
        }
    }
}
