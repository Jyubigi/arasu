﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCounter : MonoBehaviour {
    public SpriteRenderer sprite;
    public Sprite[] numSprites = new Sprite[10];
    private int countNum = 0;
    public int CountNumber
    {
        get
        {
            return countNum;
        }
        set
        {
            countNum = value;
            sprite.sprite = numSprites[countNum];
        }
    }
	// Use this for initialization
	void Awake () {
        for(int i=0; i <9; i++)
        {
            numSprites = Resources.LoadAll<Sprite>("Image/Common/Number/new");
        }
        sprite = gameObject.GetComponent<SpriteRenderer>();
    }
	public void EndRunAwayAnim()
    {
        gameObject.SetActive(false);
    }
    public void SpawnNumberAni(Vector3 _directionPos)
    {
        GameObject numAni = Instantiate <GameObject>( Resources.Load<GameObject>("Prefabs/speach"));
        numAni.transform.position = transform.position;
        numAni.transform.parent = transform;
        numAni.transform.localScale = Vector3.one;
        numAni.GetComponent<SpriteRenderer>().sprite = sprite.sprite;
        numAni.GetComponent<NumberAnimation>().StartAni(_directionPos);
    }
}