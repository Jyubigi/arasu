﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtractionManager : ChestnutManager
{
    public int t1; // 뺄수
    public int t2;//원래 수
    Chestnut[] chestnut1 = new Chestnut[9];
    int nowCount = 0;
    

    void Awake()
    {
        InitScene();
        t2 = Random.Range(6, 10);
        t1 = Random.Range(1, t2);
        sum = t2 - t1;
        GameObject c1 = GameObject.Find("Tree1").transform.GetChild(0).gameObject;
        for (int i = 0; i < 9; i++)
        {
            chestnut1[i] = c1.transform.GetChild(i).GetComponent<Chestnut>();
            if (i >= t2)
                chestnut1[i].gameObject.SetActive(false);
        }
    }
    private void Start()
    {
        StartCoroutine("GoCoin");
    }
    IEnumerator GoCoin() // 열매 애니메이션 코루 틴
    {
        // 나무에 매달린거 시각적인텀
        for (int i = 0; i < t2; i++)
        {
            chestnut1[i].goPop();
            counter.CountNumber = i + 1;
            yield return new WaitForSeconds(2f);
            
        }
        yield return new WaitForSeconds(3f);
        counter.SpawnNumberAni(new Vector3(4f, 2.62f, 0f));
        counter.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        numberFrame.addNumber(t2, Vector2.zero, mathQ.GetChild(0).GetChild(0), numImages);
        yield return new WaitForSeconds(2f);
        counter.gameObject.SetActive(true);
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].goDown(3.5f);
            yield return new WaitForSeconds(1f);
            counter.CountNumber = i + 1;
        }
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].goDraw(squirrel.transform,i);
            yield return new WaitForSeconds(1f);
        }
        
        counter.SpawnNumberAni(new Vector3(4f, 0.73f, 0f));
        yield return new WaitForSeconds(1f);
        numberFrame.addNumber(t1, Vector2.zero, mathQ.GetChild(0).GetChild(1), numImages);
        
        yield return new WaitForSeconds(1f);
        squirrel.Go(new Vector3(-4.8f, -3.78f, 0f));//바위에 숨음

        yield return new WaitForSeconds(1f);
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].transform.parent = null;
        }


        GameObject co =  (GameObject)Instantiate(counter.gameObject);// 말풍선 복제.
        co.transform.parent = squirrel.transform;
        counter.transform.parent = null;//원래 말풍선 두고감

        co.transform.Rotate(new Vector3(0f, 180f, 0f));
        co.transform.localPosition = new Vector3(-5.96f,12f,0f);
        co.transform.localScale = new Vector3(3f, 3f, 1f);
        co.gameObject.SetActive(false);
        squirrel.Back();
        yield return new WaitForSeconds(2f);
        
        for (int i = t1; i < t2; i++)
        {
            chestnut1[i].goPop();
            yield return new WaitForSeconds(2f);

        }
        co.gameObject.SetActive(true);
        co.transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Image/Buttons/question_mark");
        yield return new WaitForSeconds(1f);
        co.GetComponent<CoinCounter>().SpawnNumberAni(new Vector3(4f, -1.26f, 0f));
        yield return new WaitForSeconds(1f);
        resOb = blankFrame.addBlank(sum, Vector2.zero, mathQ.GetChild(0).GetChild(2), rightUi.GetComponentsInChildren<dragable>());
    }
}
