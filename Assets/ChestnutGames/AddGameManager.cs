﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGameManager : ChestnutManager {
    public int t1 = 1; // 첫번쨰 나무의 열매개수
    Chestnut[] chestnut1 = new Chestnut[9];
    Chestnut[] chestnut2 = new Chestnut[9];
    int nowCount = 0;

    void Awake()
    {
        InitScene();
        t1 = Random.Range(1, 8);
        sum = Random.Range(t1+2, 10);
        GameObject c1 = GameObject.Find("Tree1").transform.GetChild(0).gameObject;
        GameObject c2 = GameObject.Find("Tree2").transform.GetChild(0).gameObject;
        for (int i = 0; i < 9; i++)
        {
            chestnut1[i] = c1.transform.GetChild(i).GetComponent<Chestnut>();
            chestnut2[i] = c2.transform.GetChild(i).GetComponent<Chestnut>();
            if (i < t1)
            {
                chestnut2[i].gameObject.SetActive(false);
            }
            else
            {
                chestnut1[i].gameObject.SetActive(false);
            }
            if (i >= sum) chestnut2[i].gameObject.SetActive(false);
        }
    }
    private void Start()
    {
        Choi.SoundManager.I.ChangeBGM("GAME_MUSIC");
        StartCoroutine("GameStart");
    }
    
    IEnumerator GameStart() // 열매 애니메이션 코루 틴
    {
        counter.gameObject.SetActive(false);
        // 나무에 매달린거 시각적인텀
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].goSway();
        }
        for (int i = t1; i < sum; i++)
        {
            chestnut2[i].goSway();
        }
        yield return new WaitForSeconds(3f);

        //1.나무에서 열매가 하나씩 떨어짐
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].goNone();
            chestnut1[i].goDown(3.5f);
            yield return new WaitForSeconds(1f);
        }
        counter.gameObject.SetActive(true);
        //2.다람쥐가 숫자를 셈
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].goDraw(transform,i,2, 0.3f,0.2f);
            yield return new WaitForSeconds(1f);
            counter.CountNumber = i + 1;
        }
        //3.다람쥐가 열매 아래에 가져다놓음(한꺼번에)
        squirrel.Go(new Vector3(-4.53f, -3.7f, 0f));
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < t1; i++)
        {
            chestnut1[i].transform.parent = null;
            chestnut1[i].goByLine(new Vector3(-4.53f, -3.7f, 0f), i);
            yield return new WaitForSeconds(1f);
        }
        
        ///4.다람쥐가 수식 가져다 놓음
        counter.SpawnNumberAni(new Vector3(4f,2.8f,0f));
        yield return new WaitForSeconds(1f);
        numberFrame.addNumber(t1, Vector2.zero, mathQ.GetChild(0).GetChild(0), numImages);
        yield return new WaitForSeconds(1f);
        counter.gameObject.SetActive(false);
        squirrel.Back();
        yield return new WaitForSeconds(1f);
        
        squirrel.Go(new Vector3(-1.31f, 0.02f, 0f));

        yield return new WaitForSeconds(1f);
        // (다음나무)
        //1.나무에서 열매가 하나씩 떨어짐
        for (int i = t1; i < sum; i++)
        {
            chestnut2[i].goNone();
            chestnut2[i].goDown(3.5f);
            yield return new WaitForSeconds(1f);
        }
        counter.CountNumber = 0;
        counter.gameObject.SetActive(true);
        //2.다람쥐가 숫자를 셈
        for (int i = t1; i < sum; i++)
        {
            chestnut2[i].goDraw(transform, i,2,0.3f,0.2f);
            yield return new WaitForSeconds(1f);
            counter.CountNumber = i + 1- t1;
        }
        //3.다람쥐가 열매 아래에 가져다놓음(한꺼번에)
        squirrel.Go(new Vector3(-4.53f + (float)t1, -3.7f, 0f));
        yield return new WaitForSeconds(3f);
        for (int i = t1; i < sum; i++)
        {
            chestnut2[i].transform.parent = null;
            chestnut2[i].goByLine(new Vector3(-4.53f, -3.7f, 0f), i);
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(1f);
        ///4.다람쥐가 수식 가져다 놓음
        counter.SpawnNumberAni(new Vector3(4f, 1.1f, 0f));
        yield return new WaitForSeconds(1f);
        numberFrame.addNumber(sum - t1, Vector2.zero, mathQ.GetChild(0).GetChild(1), numImages);

        yield return new WaitForSeconds(1f);
        counter.gameObject.SetActive(false);

        //5.마지막 다람쥐 수풀 바로옆 하단 -다람쥐가 숫자를 세지만 답은 안나옴. 아래쪽 열매가 커졌다 작아졌다하면 카운트다운 효과음
        squirrel.Go(new Vector3(-5.11f, -3.74f, 0f));
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < sum; i++)
        {
            if (i < t1)
                chestnut1[i].goPop();
            else
                chestnut2[i].goPop();

            yield return new WaitForSeconds(2f);
        }
        yield return new WaitForSeconds(1f);
        counter.gameObject.SetActive(true);
        counter.transform.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Image/Buttons/question_mark");
        yield return new WaitForSeconds(1f);
        counter.GetComponent<CoinCounter>().SpawnNumberAni(new Vector3(4f, -1f, 0f));
        yield return new WaitForSeconds(1f);
        resOb = blankFrame.addBlank(sum, Vector2.zero, mathQ.GetChild(0).GetChild(2), rightUi.GetComponentsInChildren<dragable>());
    }
}
