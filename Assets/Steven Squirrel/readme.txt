Steven Squirrel


/* ABOUT */
Meet Steven Squirrel the friendly adventurous character of your next amazing game.

Make Steven:
- Run, as fast as his short legs will carry him.
- Jump, as high as possible.
- Idle, for those brief moments of relaxation.

Features:
- Hand drawn art.
- Three animations (run, jump, idle).
- Amazing, live like, motion.
- PSD asset file for easy editing.
- Love.


/* GET STARTED */
- Open the Demo scene in the Scenes folder to edit or copy the existing animation.


/* WEBSITE *
http://davidochmann.de